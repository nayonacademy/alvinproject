from celery.decorators import task
import os, time
import ssl, json, requests, urllib
from django.core.mail import EmailMessage
from celery import current_task
import subprocess
from django.conf import settings
from celery.utils.log import get_task_logger
logger = get_task_logger(__name__)

def root_url():
    base_dir = settings.BASE_DIR.strip()
    root_dir = "/".join(base_dir.split("/")[1:-1])
    final_dir = "/" + root_dir
    return final_dir

@task(name="timerange_task")
def scrapper_task(filename):
    sh_url = os.path.join(root_url(),"scrapper/"+filename)
    subprocess.call(sh_url)
    print("task status check")
