from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('scrapper/list/', views.scrapperlist, name='scrapperlist'),
    path('flushdatabase/', views.flushdatabase, name='flushdatabase'),
    path('allscraper/', views.allscraper, name='allscraper'),
    path('allhyips/', views.allhyips, name='allhyips'),
    path('elitemonitor/', views.elitemonitor, name='elitemonitor'),
    path('fairmonitor/', views.fairmonitor, name='fairmonitor'),
    path('graspgold/', views.graspgold, name='graspgold'),
    path('hyipclub/', views.hyipclub, name='hyipclub'),
    path('hyipcruiser/', views.hyipcruiser, name='hyipcruiser'),
    path('investtracing/', views.investtracing, name='investtracing'),
    path('myhips/', views.myhips, name='myhips'),
    path('myinvestblog/', views.myinvestblog, name='myinvestblog'),
    path('uhyips/', views.uhyips, name='uhyips'),
    path('timerng/', views.timerng, name='timerng'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
