from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.shortcuts import render
import pymongo
from django.shortcuts import render_to_response
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
import json

from adminpanel.tasks import scrapper_task

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["hyip_1"]
mycol = mydb["scrapper"]
from datetime import date as D
import difflib
from operator import itemgetter
# Create your views here.
from django.urls import reverse
from django.conf import settings
import os

def root_url():
    base_dir = settings.BASE_DIR.strip()
    root_dir = "/".join(base_dir.split("/")[1:-1])
    final_dir = "/" + root_dir
    return final_dir

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("dashboard"))
        else:
            return render(request, 'adminpanel/login.html', {'message': 'Invalid access'})
    else:
        return render(request, 'adminpanel/login.html')


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("login"))


@login_required
def dashboard(request):
    return render(request, 'adminpanel/admin_dashboard.html')


@login_required
def scrapperlist(request):
    contact_list = mycol.find().sort([('created_date', -1)])
    final = []
    final_pag = []
    for i in contact_list:
        id = i.get('id')
        name = i.get('name')
        url = i.get('url')
        #     $thumbnail = $value['thumbnail'];
        last_paid = i.get('lastpaid')
        if i['created_date'] != 'N/A':
            created_date = i.get('created_date')
            lifetime = str((D.today() - created_date.date())).split(',')[0]
        else:
            created_date = 'n/a'
            lifetime = 'n/a'

        takes = i.get('takes')
        if i.get('withdrawtype'):
            withdrawtype = i.get('withdrawtype')
        else:
            withdrawtype = 'n/a'

        min_depo = i.get('Min/Max')
        status = i.get('status')
        if i.get('investmentplan'):
            investment_plan = i.get('investmentplan')
        else:
            investment_plan = 'n/a'

        if i.get('valid'):
            ssl_valid = i.get('valid')
        else:
            ssl_valid = 'n/a'

        if i.get('issuerbrand'):
            if type(['issuerbrand']) == list():
                issuer_brand = i['issuerbrand'][0]
            else:
                issuer_brand = i['issuerbrand']
        else:
            issuer_brand = ''

        if i.get('validation_type'):
            validation_type = i['validation_type']
        else:
            validation_type = 'n/a'

        if i['script']:
            script = i['script']
        else:
            script = 'n/a'

        if i['datacenter']:
            datacenter = i['datacenter']
        else:
            datacenter = 'n/a'

        if i.get('ip'):
            ip = i['ip']
        else:
            ip = 'n/a'

        if i.get('servers'):
            servers = i['servers']
        else:
            servers = 'n/a'
        if i.get('status1'):
            status1 = i['status1']
        else:
            status1 = ''
        if i.get('status2'):
            status2 = i['status2']
        else:
            status2 = ''

        forums = []
        if i.get('forums'):
            forum = i['forums']
            for x in forum:
                link = x
                name = x.split("//")[1].split("/")[0]
                forums.append({"link": link, "name": name})

        if i.get('investmentplan'):
            investmentplan = i['investmentplan']
        else:
            investmentplan = 'N/A'

        monitors = ['fairmonitor', 'graspgold', 'investtracing', 'myhyips', 'hyipclub', 'allhyips', 'uhyips',
                    'hyipcruiser', 'elitemonitor']

        paying = 0
        waiting = 0
        problem = 0
        notpaying = 0
        monitors_count = 0
        rcbs_data = []
        for m in monitors:
            if i.get('RCB_data_' + m):
                rcbs_data.append('RCB_data_' + m)

            if i.get('status_' + m):
                monitors_count += 1
                if (i.get(str('status_' + m))).lower() == 'paying':
                    paying = int(paying) + 1
                else:
                    pass

                if (i.get('status_' + m)).lower() == 'waiting':
                    waiting = waiting + 1
                else:
                    pass

                if (i.get('status_' + m)).lower() == 'problem':
                    problem = problem + 1
                else:
                    pass

                if (i.get('status_' + m)).lower() == 'notpaying':
                    notpaying = notpaying + 1
                else:
                    pass
            else:
                pass

        total_deposit = 0
        for r in rcbs_data:
            if r:
                for a in i.get(r):
                    total_deposit += float(a['depo'].replace(',', ''))

        final.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                      'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
                      'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
                      'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                      'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                      'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
                      'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,
                      'monitors_count': monitors_count,
                      'lifetime': lifetime, "total_deposit": total_deposit})

        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1

        p = Paginator(final, 10, request=request)
        final_pag = p.page(page)

    top_prog = sorted(final, key=itemgetter('total_deposit'), reverse=True)
    return render(request, 'adminpanel/scrapper_list.html',{'result': final_pag, 'top_prog': top_prog[:20]})


def flushdatabase(request):
    mycol.delete_many({})
    return HttpResponseRedirect(reverse("dashboard"))


import subprocess
def allscraper(request):
    # sh_url = os.path.join(root_url(),"scrapper/runspiders.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("runspiders.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def allhyips(request):
    # sh_url = os.path.join(root_url(),"scrapper/allhyips.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("allhyips.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def elitemonitor(request):
    # sh_url = os.path.join(root_url(),"scrapper/elitemonitor.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("elitemonitor.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def fairmonitor(request):
    # sh_url = os.path.join(root_url(),"scrapper/fairmonitor.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("fairmonitor.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def graspgold(request):
    # sh_url = os.path.join(root_url(),"scrapper/graspgold.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("graspgold.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def hyipclub(request):
    # sh_url = os.path.join(root_url(),"scrapper/hyipclub.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("hyipclub.sh")
    return HttpResponseRedirect(reverse("dashboard"))

# Test area
def hyipcruiser(request):
    # sh_url = os.path.join(root_url(),"scrapper/hyipcruiser.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("hyipcruiser.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def investtracing(request):
    # sh_url = os.path.join(root_url(),"scrapper/investtracing.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("investtracing.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def myhips(request):
    # sh_url = os.path.join(root_url(),"scrapper/myhips.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("myhips.sh")
    return HttpResponseRedirect(reverse("dashboard"))


def myinvestblog(request):
    # sh_url = os.path.join(root_url(),"scrapper/myinvestblog.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("myinvestblog.sh")
    return HttpResponseRedirect(reverse("dashboard"))

def uhyips(request):
    # sh_url = os.path.join(root_url(),"scrapper/uhyips.sh")
    # subprocess.call(sh_url)
    scrapper_task.delay("uhyips.sh")
    return HttpResponseRedirect(reverse("dashboard"))



def timerng(request):
    """
    Deprecated
    :param request:
    :return:
    """
    mytask = scrapper_task.delay("finename")
    print(mytask)
    return HttpResponse("I am here")
