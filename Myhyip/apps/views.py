from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
import pymongo
from django.shortcuts import render_to_response
from django.urls import reverse
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
import json
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
# myclient = pymongo.MongoClient("mongodb://mongo:47256/")
mydb = myclient["hyip_1"]
mycol = mydb["scrapper"]
from datetime import date as D
import difflib
from operator import itemgetter
from datetime import datetime, date,timedelta


# Create your views here.
def index(request):
    contact_list = mycol.find().sort([('created_date', -1)])
    final = []
    final_pag = ""
    today_final = []
    yesterday_final = []
    for i in contact_list:
        id = i.get('id')
        name = i.get('name')
        url = i.get('url')
        last_paid = i.get('lastpaid')
        if i['created_date'] != 'N/A':
            created_date = i.get('created_date')
            lifetime = str((D.today() - created_date.date())).split(',')[0]
        else:
            created_date = 'n/a'
            lifetime = 'n/a'
        takes = i.get('takes')
        if i.get('withdrawtype'):
            withdrawtype = i.get('withdrawtype')
        else:
            withdrawtype = 'n/a'

        min_depo = i.get('Min/Max')
        status = i.get('status')
        if i.get('investmentplan'):
            investment_plan = i.get('investmentplan')
        else:
            investment_plan = 'n/a'

        if i.get('valid'):
            ssl_valid = i.get('valid')
        else:
            ssl_valid = 'n/a'

        if i.get('issuerbrand'):
            if type(['issuerbrand']) == list():
                issuer_brand = i['issuerbrand'][0]
            else:
                issuer_brand = i['issuerbrand']
        else:
            issuer_brand = ''

        if i.get('validation_type'):
            validation_type = i['validation_type']
        else:
            validation_type = 'n/a'

        if i.get('script'):
            script = i['script']
        else:
            script = 'n/a'

        if i.get('datacenter'):
            datacenter = i['datacenter']
        else:
            datacenter = 'n/a'

        if i.get('ip'):
            ip = i['ip']
        else:
            ip = 'n/a'

        if i.get('servers'):
            servers = i['servers']
        else:
            servers = 'n/a'
        if i.get('status1'):
            status1 = i['status1']
        else:
            status1 = ''
        if i.get('status2'):
            status2 = i['status2']
        else:
            status2 = ''

        forums = []
        if i.get('forums'):
            forum = i['forums']
            for x in forum:
                link = x
                name = x.split("//")[1].split("/")[0]
                forums.append({"link": link, "name": name})

        if i.get('investmentplan'):
            investmentplan = i['investmentplan']
        else:
            investmentplan = 'N/A'

        monitors = ['fairmonitor', 'graspgold', 'investtracing', 'myhyips', 'hyipclub', 'allhyips', 'uhyips','hyipcruiser', 'elitemonitor']

        paying = 0
        waiting = 0
        problem = 0
        notpaying = 0
        monitors_count = 0
        rcbs_data = []
        for m in monitors:
            if i.get('RCB_data_'+m):
                rcbs_data.append('RCB_data_'+m)

            if i.get('status_'+m):
                monitors_count +=1
                if (i.get(str('status_' + m))).lower() == 'paying':
                    paying = int(paying) + 1
                else:
                    pass

                if (i.get('status_'+m)).lower() == 'waiting':
                    waiting = waiting + 1
                else:
                    pass

                if (i.get('status_'+m)).lower() == 'problem':
                    problem = problem + 1
                else:
                    pass

                if (i.get('status_'+m)).lower() == 'notpaying':
                    notpaying = notpaying + 1
                else:
                    pass
            else:
                pass

        total_deposit = 0
        for r in rcbs_data:
            if r:
                for a in i.get(r):
                    total_deposit +=float(a['depo'].replace(',', ''))

        final.append({'id': id,'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
         'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
         'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
         'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
         'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
         'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
        'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,'monitors_count': monitors_count,
        'lifetime':lifetime,"total_deposit": total_deposit})

        if i['created_date'] != 'N/A':
            created_date = i.get('created_date')
            if created_date.date() > datetime.today().date():
                today_final.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                              'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
                              'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
                              'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                              'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                              'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
                              'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,
                              'monitors_count': monitors_count,
                              'lifetime': lifetime, "total_deposit": total_deposit})

            yesterday = date.today() - timedelta(1)
            if yesterday > datetime.today().date():
                yesterday_final.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                              'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
                              'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
                              'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                              'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                              'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
                              'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,
                              'monitors_count': monitors_count,
                              'lifetime': lifetime, "total_deposit": total_deposit})

        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1

        p = Paginator(final, 10, request=request)
        final_pag = p.page(page)

    top_prog = sorted(final, key=itemgetter('total_deposit'), reverse=True)
    top_today = sorted(today_final, key=itemgetter('total_deposit'), reverse=True)
    top_yesterday = sorted(yesterday_final, key=itemgetter('total_deposit'), reverse=True)
    return render(request, 'apps/index.html', {'result': final_pag, 'top_prog': top_prog[:20], 'top_today': top_today[:20], 'top_yesterday': top_yesterday[:20]})


def search(request):
    search_text = request.GET.get('hyip', None)
    print("search test :: ",search_text)
    contact_list = mycol.find().sort([('created_date', -1)])
    contact_list = mycol.find({$text: {$search: "\"coffee shop\""}}).sort([('created_date', -1)])
    final = []
    final_pag = ""
    today_final = []
    yesterday_final = []
    for i in contact_list:
        id = i.get('id')
        name = i.get('name')
        url = i.get('url')
        last_paid = i.get('lastpaid')
        if i['created_date'] != 'N/A':
            created_date = i.get('created_date')
            lifetime = str((D.today() - created_date.date())).split(',')[0]
        else:
            created_date = 'n/a'
            lifetime = 'n/a'
        takes = i.get('takes')
        if i.get('withdrawtype'):
            withdrawtype = i.get('withdrawtype')
        else:
            withdrawtype = 'n/a'

        min_depo = i.get('Min/Max')
        status = i.get('status')
        if i.get('investmentplan'):
            investment_plan = i.get('investmentplan')
        else:
            investment_plan = 'n/a'

        if i.get('valid'):
            ssl_valid = i.get('valid')
        else:
            ssl_valid = 'n/a'

        if i.get('issuerbrand'):
            if type(['issuerbrand']) == list():
                issuer_brand = i['issuerbrand'][0]
            else:
                issuer_brand = i['issuerbrand']
        else:
            issuer_brand = ''

        if i.get('validation_type'):
            validation_type = i['validation_type']
        else:
            validation_type = 'n/a'

        if i.get('script'):
            script = i['script']
        else:
            script = 'n/a'

        if i.get('datacenter'):
            datacenter = i['datacenter']
        else:
            datacenter = 'n/a'

        if i.get('ip'):
            ip = i['ip']
        else:
            ip = 'n/a'

        if i.get('servers'):
            servers = i['servers']
        else:
            servers = 'n/a'
        if i.get('status1'):
            status1 = i['status1']
        else:
            status1 = ''
        if i.get('status2'):
            status2 = i['status2']
        else:
            status2 = ''

        forums = []
        if i.get('forums'):
            forum = i['forums']
            for x in forum:
                link = x
                name = x.split("//")[1].split("/")[0]
                forums.append({"link": link, "name": name})

        if i.get('investmentplan'):
            investmentplan = i['investmentplan']
        else:
            investmentplan = 'N/A'

        monitors = ['fairmonitor', 'graspgold', 'investtracing', 'myhyips', 'hyipclub', 'allhyips', 'uhyips','hyipcruiser', 'elitemonitor']

        paying = 0
        waiting = 0
        problem = 0
        notpaying = 0
        monitors_count = 0
        rcbs_data = []
        for m in monitors:
            if i.get('RCB_data_'+m):
                rcbs_data.append('RCB_data_'+m)

            if i.get('status_'+m):
                monitors_count +=1
                if (i.get(str('status_' + m))).lower() == 'paying':
                    paying = int(paying) + 1
                else:
                    pass

                if (i.get('status_'+m)).lower() == 'waiting':
                    waiting = waiting + 1
                else:
                    pass

                if (i.get('status_'+m)).lower() == 'problem':
                    problem = problem + 1
                else:
                    pass

                if (i.get('status_'+m)).lower() == 'notpaying':
                    notpaying = notpaying + 1
                else:
                    pass
            else:
                pass

        total_deposit = 0
        for r in rcbs_data:
            if r:
                for a in i.get(r):
                    total_deposit +=float(a['depo'].replace(',', ''))

        final.append({'id': id,'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
         'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
         'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
         'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
         'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
         'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
        'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,'monitors_count': monitors_count,
        'lifetime':lifetime,"total_deposit": total_deposit})

        if i['created_date'] != 'N/A':
            created_date = i.get('created_date')
            if created_date.date() > datetime.today().date():
                today_final.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                              'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
                              'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
                              'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                              'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                              'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
                              'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,
                              'monitors_count': monitors_count,
                              'lifetime': lifetime, "total_deposit": total_deposit})

            yesterday = date.today() - timedelta(1)
            if yesterday > datetime.today().date():
                yesterday_final.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                              'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
                              'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
                              'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                              'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                              'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
                              'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,
                              'monitors_count': monitors_count,
                              'lifetime': lifetime, "total_deposit": total_deposit})

        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1

        p = Paginator(final, 10, request=request)
        final_pag = p.page(page)

    top_prog = sorted(final, key=itemgetter('total_deposit'), reverse=True)
    top_today = sorted(today_final, key=itemgetter('total_deposit'), reverse=True)
    top_yesterday = sorted(yesterday_final, key=itemgetter('total_deposit'), reverse=True)
    return render(request, 'apps/index.html', {'result': final_pag, 'top_prog': top_prog[:20], 'top_today': top_today[:20], 'top_yesterday': top_yesterday[:20]})


def detailspage(request):
    id = request.GET.get('id', None)
    name = ''
    final = []
    detail = mycol.find({'url': id})
    today_final = []
    yesterday_final = []
    monitors = ['fairmonitor', 'graspgold', 'investtracing', 'myhyips', 'hyipclub', 'allhyips', 'uhyips',
                 'hyipcruiser', 'elitemonitor', 'myinvestblog']
    monitors_url = ['fairmonitor.com', 'graspgold.com', 'invest-tracing.com', 'myhyips.net', 'hyipclub.club',
                     'all-hyips.info', 'uhyips.com', 'hyip-cruiser.com', 'elite-monitor.com']
    rcbs = []
    rcbs_data = []
    dates_depo = []
    total = 0
    total_per_day = 0
    users = []
    depos = []
    counter = 0
    monitors_count = 0
    details = []
    button = ''
    buttons = ''
    buttons_url = ''
    for i in detail:
        button = []
        buttons_url = []
        paying = 0
        waiting = 0
        problem = 0
        notpaying = 0
        for m in monitors:
            if i.get('button_'+m):
                mbutton = i['button_'+m]
                btnurl = difflib.get_close_matches(m, monitors_url)
                buttons_url.append(btnurl[0])
                if type(i.get('button_'+m)) == list():
                    button.append(['button_'+m][0])
                else:
                    button.append('button_' + m)

            # RCB_monitor name get in list
            if i.get('RCB_'+m):
                monitors_count += 1
                rcbs.append('RCB_'+m)

            # RCB_data_monitor name get in list
            if i.get('RCB_data_'+m):
                rcbs_data.append('RCB_data_'+m)

            if i.get(str('status_' + m)):
                if (i.get(str('status_' + m))).lower() == 'paying':
                    paying = int(paying) + 1
                else:
                    pass

            if i.get('status_' + m):
                if (i.get('status_'+m)).lower() == 'waiting':
                    waiting = waiting + 1
                else:
                    pass

            if i.get('status_' + m):
                if (i.get('status_'+m)).lower() == 'problem':
                    problem = problem + 1
                else:
                    pass

            if i.get('status_' + m):
                if (i.get('status_'+m)).lower() == 'notpaying':
                    notpaying = notpaying + 1
                else:
                    pass

        if i.get('buttons'):
            buttons = i.get('buttons')

        url = i.get('url')

        last_paid = i.get('lastpaid')
        if i.get('created_date') != 'N/A':
            created_date = i.get('created_date')
            lifetime = str((D.today() - created_date.date())).split(',')[0]
        else:
            created_date = 'n/a'
            lifetime = 'n/a'

        takes = i.get('takes')

        if i.get('withdrawtype'):
            withdrawl = i.get('withdrawtype')
        else:
            withdrawl = 'n/a'

        min_depo = i.get('Min/Max')
        status = i.get('status')

        if i.get('investmentplan'):
            investmentplan = i.get('investmentplan')
        else:
            investmentplan = 'n/a'

        if i.get('valid'):
            ssl_valid = i.get('valid')
        else:
            ssl_valid = 'n/a'

        if i.get('issuerbrand'):
            if type(['issuerbrand']) == list():
                issuer_brand = i['issuerbrand'][0]
            else:
                issuer_brand = i['issuerbrand']
        else:
            issuer_brand = ''

        if i.get('validation_type'):
            validation_type = i['validation_type']
        else:
            validation_type = 'n/a'

        if i['script']:
            script = i['script']
        else:
            script = 'n/a'

        if i['datacenter']:
            datacenter = i['datacenter']
        else:
            datacenter = 'n/a'

        if i.get('ip'):
            ip = i['ip']
        else:
            ip = 'n/a'

        if i.get('servers'):
            servers = i['servers']
        else:
            servers = 'n/a'
        if i.get('status1'):
            status1 = i['status1']
        else:
            status1 = ''
        if i.get('status2'):
            status2 = i['status2']
        else:
            status2 = ''

        forums = []
        if i.get('forums'):
            forum = i['forums']
            for x in forum:
                link = x
                name = x.split("//")[1].split("/")[0]
                forums.append({"link": link, "name": name})
        details.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                  'takes': takes, 'withdrawl': withdrawl, 'min_depo': min_depo,
                  'status': status, 'investment_plan': investmentplan, 'ssl_valid': ssl_valid,
                  'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                  'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                  'status2': status2, 'forums': forums, 'investmentplan': investmentplan,
                  'lifetime': lifetime, 'rcbs': rcbs, 'rcbs_data': rcbs_data,
                  'button': button, 'buttons': buttons, 'buttons_url': buttons_url,
                  'monitors_count': monitors_count,'mbutton': mbutton.decode("utf-8") })

    name = details[0]['url']
    total_deposit = 0
    num_depositors = 0
    num_investors = 0
    top_depo = []
    rcb_list = []
    investor_list = []
    for r in rcbs_data:
        if r:
            for a in i.get(r):
                print(a)
                investor_list.append(a['user'])
                top_depo.append({'date': a['date'], 'depo': float(a['depo']), 'user': a['user']})
                num_depositors += 1
                total_deposit += float(a['depo'].replace(',', ''))
    num_investors = len(set(investor_list))
    for r in rcbs:
        if r:
            for b in i.get(r):
                rcb_list.append({'min': b['min'], 'max': b['max'], '1stDepRCB': b['1stDepRCB'], 'redep_rcb': b['redep-rcb'], 'percentage': b['percentage'], '1stbonus': b['1stbonus'], 'redep_bonus': b['redep-bonus']})

    # top list
    contact_list = mycol.find().sort([('created_date', -1)])
    final = []
    for i in contact_list:
        id = i.get('id')
        name = i.get('name')
        url = i.get('url')
        monitors = ['fairmonitor', 'graspgold', 'investtracing', 'myhyips', 'hyipclub', 'allhyips', 'uhyips',
                    'hyipcruiser', 'elitemonitor']

        paying = 0
        waiting = 0
        problem = 0
        notpaying = 0
        monitors_count = 0
        rcbs_data = []
        for m in monitors:
            if i.get('RCB_data_' + m):
                rcbs_data.append('RCB_data_' + m)

        total_deposit = 0
        for r in rcbs_data:
            if r:
                for a in i.get(r):
                    total_deposit += float(a['depo'].replace(',', ''))

        final.append({'id': id, 'name': name, 'url': url, "total_deposit": total_deposit})
        if i['created_date'] != 'N/A':
            created_date = i.get('created_date')
            if created_date.date() > datetime.today().date():
                today_final.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                              'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
                              'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
                              'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                              'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                              'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
                              'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,
                              'monitors_count': monitors_count,
                              'lifetime': lifetime, "total_deposit": total_deposit})

            yesterday = date.today() - timedelta(1)
            if yesterday > datetime.today().date():
                yesterday_final.append({'id': id, 'name': name, 'url': url, 'last_paid': last_paid, 'created_date': created_date,
                              'takes': takes, 'withdrawtype': withdrawtype, 'min_depo': min_depo,
                              'status': status, 'investment_plan': investment_plan, 'ssl_valid': ssl_valid,
                              'issuer_brand': issuer_brand, 'validation_type': validation_type, 'script': script,
                              'datacenter': datacenter, 'ip': ip, 'servers': servers, 'status1': status1,
                              'status2': status2, 'forums': forums, 'investmentplan': investmentplan, 'paying_c': paying,
                              'waiting_c': waiting, 'problem_c': problem, 'notpaying_c': notpaying,
                              'monitors_count': monitors_count,
                              'lifetime': lifetime, "total_deposit": total_deposit})

    top_prog = sorted(final, key=itemgetter('total_deposit'), reverse=True)

    top_today = sorted(today_final, key=itemgetter('total_deposit'), reverse=True)
    top_yesterday = sorted(yesterday_final, key=itemgetter('total_deposit'), reverse=True)
    top_depo_l = sorted(top_depo, key=itemgetter('depo'),reverse=True)
    context = {
        'result': details,
        'name': name,
        'total_deposit': total_deposit,
        'num_depositors': num_depositors,
        'num_investors': num_investors,
        'top_depo': top_depo_l[:20],
        'rcb_list': rcb_list,
        'paying': paying,
        'waiting': waiting,
        'problem': problem,
        'notpaying': notpaying,
        'table_id': name,
        'top_prog': top_prog[:20],
        'top_today': top_today,
        'top_yesterday':top_yesterday
    }
    return render(request, 'apps/programdetails.html', context)
