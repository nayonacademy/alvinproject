FROM python:3.6.5-onbuild

RUN apt-get update
RUN apt-get install -y libmysqlclient-dev mysql-client sqlite3 libpq-dev libffi-dev

ENV PYTHONUNBUFFERED 1
COPY . /var/www/html/alvin/requirements.txt
WORKDIR /var/www/html/alvin/