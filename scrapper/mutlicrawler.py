import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from hyipscrawl.spiders import fairmonitor
from hyipscrawl.spiders import elitemonitor
from hyipscrawl.spiders import allhyips
from hyipscrawl.spiders import graspgold

process = CrawlerProcess(get_project_settings())
process.crawl('elitemonitorlean')
process.crawl('allhyipslean')
process.crawl('fairmonitorlean')
process.crawl('graspgoldlean')
process.crawl('hyipcruiserlean')
process.crawl('hyipclublean')
process.crawl('investtracinglean')
process.crawl('myhyipslean')
process.crawl('uhyipslean')
process.crawl('myinvestbloglean')
process.start()