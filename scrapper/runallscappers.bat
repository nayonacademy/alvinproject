@echo off
for %%x in (
        allhyips
        elitemonitor
	    fairmonitor
        graspgold
		hyipclub 
		hyipcruiser
		investtracing
		myhips
		uhyips
		myinvestblog
       ) do (
         echo starting scapper %%x
         scrapy crawl %%x
         echo =-=-=-=-=-=
         echo.
       )