import pymongo
import logging

class MongoPipeline(object):

    collection_name = 'scrapper'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('mongodb://127.0.0.1:27017/'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'hyip_1')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        spider.client = self.client
        print(spider.name)
        self.db = self.client[self.mongo_db]
        spider.db = self.db
        spider.collection_name = self.collection_name


    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        if(item['url'] == ''):
            print('error fetching url')
            return item
        dup_check = self.db[self.collection_name].find({'url': item['url']}).count()
        if('lean' in spider.name and dup_check > 0):
            self.db[self.collection_name].update({'url': item['url']}, {'$set': item }, upsert=False)
            print('hyip updated by lean spider')
            return item
        elif ('lean' in spider.name and dup_check == 0):
            return 'new hyip'
        else:
            if dup_check == 0:
                self.db[self.collection_name].insert_one(dict(item))
                print("hyip added to MongoDB database!")
            else:
                updated_data = {}
                if ('lastpaid' in item):
                    updated_data['lastpaid'] = item['lastpaid']
                if ('forums' in item):
                    updated_data['forums'] = item['forums']
                if ('status1' in item):
                    updated_data['status1'] = item['status1']
                if('status2' in item):
                    updated_data['status2'] = item['status2']
                if('valid' in item):
                    updated_data['valid'] = item['valid']
                if ('button_' + spider.name in item):
                    updated_data['button_' + spider.name] = item['button_' + spider.name]
                if ('status_' + spider.name in item):
                    updated_data['status_' + spider.name] = item['status_' + spider.name]
                if('RCB_'+spider.name in item):
                    updated_data['RCB_'+spider.name] = item['RCB_'+spider.name]
                if('RCB_data_'+spider.name in item):
                    updated_data['RCB_data_' + spider.name] = item['RCB_data_' + spider.name]
                self.db[self.collection_name].update({'url': item['url']}, {'$set': updated_data}, upsert=True)
                print('hyip updated')
            return item