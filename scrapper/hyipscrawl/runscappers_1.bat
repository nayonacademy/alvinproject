@echo off
for %%x in (
        fairmonitor
        hyipclub
        investtracing
        myhyips
        graspgold
       ) do (
         echo starting scapper %%x
         scrapy crawl %%x
         echo =-=-=-=-=-=
         echo.
       )