# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider



class fairmonitor(BaseSpider):
    name = 'fairmonitor'
    allowed_domains = ['fairmonitor.com']
    start_urls = ['https://fairmonitor.com/?lang=fr']

    def start_requests(self):
        urls = [
            'https://fairmonitor.com/?lang=fr',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.prog-folder')
        for div in divs:
            item = {}
            # try:
            id = div.css('li.name a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://fairmonitor.com/go/lid/' + id)
            item['name']  = div.css('li.name a::text').extract_first().strip()#item['url'].split('/')[2]
            if (self.check_new(item['url'])):
                self.generate_thumbnail(item['url'], item['name'])
                item['script'] = self.get_script(item['url'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                item['status'] = div.css('li.status span::text').extract_first().strip()
                item['status_fairmonitor'] = div.css('li.status span::text').extract_first().strip()
                item['lastpaid'] = div.css('li.last::text').extract_first().strip().strip()
                item['investmentplan'] = div.css('div.plan-info p::text').extract_first().strip().encode('utf-8').decode(
                    'unicode_escape')
                # item['startDate'] =  datetime.serial_date_to_string(div.css('div.a-p2 li:nth-of-type(0) b::text').extract_first().strip().replace('days',''))
                item['Min/Max'] = div.css('div.a-p2 li:nth-of-type(3) b::text').extract_first().strip()
                item['withdrawtype'] = div.css('div.a-p2 li:nth-of-type(4) b::text').extract_first().strip()
                item['takes'] = self.get_takes('https://fairmonitor.com/details/lid/' + id + '/?lang=fr')
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                item['button_fairmonitor'] = self.get_code('https://fairmonitor.com/get_code/lid/' + id)[0].encode(
                    'utf-8')
                resp = requests.get('https://fairmonitor.com/refback/lid/' + id + '/?lang=fr')
                item['RCB_fairmonitor'] = self.get_RCB_classformnosize(resp)
                item['RCB_data_fairmonitor'] = self.get_RCB_data_classlist(resp)
                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']
            # except:
            #     pass

                yield item

    def get_takes(self, url):
        resp = requests.get(url)
        takes = []
        response = BeautifulSoup(resp.text, features='lxml')
        for h in response.find('li', {'class': 'bo'}).find('div').find_all('img'):
            takes.append(h.attrs['title'])
        return takes

