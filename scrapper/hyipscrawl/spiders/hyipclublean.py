# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class hyipclublean(BaseSpider):
    name = 'hyipclublean'
    allowed_domains = ['hyipclub.club']
    start_urls = ['http://hyipclub.club/']

    def start_requests(self):
        urls = [
            'http://hyipclub.club/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.khunglon')
        for div in divs:
            item = {}
            #try:
            id = div.css('div.nameprogram a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://hyipclub.club/go/lid/'+id)
            item['status'] = div.css('div.k-status span::text').extract_first().strip()
            item['status_hyipclub'] = div.css('div.k-status span::text').extract_first().strip()
            item['lastpaid'] = 'n/a'#div.css('div.khunggia3 div.k-mod span.label::text').extract_first().strip()
            item['button_hyipclub'] = self.get_code('https://hyipclub.club/get_code/lid/' + id)[0].encode(
                'utf-8')

            resp = requests.get('https://hyipclub.club/refback/lid/'+id)
            item['RCB_hyipclub'] = self.get_RCB_classformnosize(resp)
            item['RCB_data_hyipclub'] = self.get_RCB_data_classlist(resp)
            #except:
             #   pass

            yield item
