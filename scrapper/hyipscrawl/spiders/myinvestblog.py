# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class myinvestblog(BaseSpider):
    name = 'myinvestblog'
    allowed_domains = ['myinvestblog.ru']
    start_urls = ['http://myinvestblog.ru/?lang=en']

    def start_requests(self):
        urls = [
            'http://myinvestblog.ru/?lang=en',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # table = response.xpath('/html/body/center/div[4]/table[2]/tbody/tr/td[2]/table[7]')

        divs = response.xpath('//div[@style="max-width:780px;"]')
        for div in divs:
            item = {}
            id = div.css('a.list-aname::attr(href)').extract_first()
            item['url'] = self.get_url('http://myinvestblog.ru/' + id)
            if (self.check_new(item['url'])):
                item['name']= div.css('a.list-aname::text').extract_first().strip()
                self.generate_thumbnail(item['url'], item['name'])
                item['script'] = self.get_script(item['url'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']
                tds_1 = div.css('div.list2 table td')
                status_img = tds_1[1].css('img::attr(src)').extract_first()
                if('_1' in status_img):
                    item['status'] = 'PAYING'
                elif('_2' in status_img):
                    item['status'] = 'WAITING'
                else:
                    item['status'] = 'PROBLEM'
                item['Min/Max'] = tds_1[1].css('b::text').extract_first().strip()
                item['withdrawtype'] = tds_1[1].css('b::text').extract()[3].strip()
                item['status_myinvestblog'] = item['status']
                #item['lastpaid']
                takes = []
                imgs = div.css('img.list_eg::attr(src)').extract()
                for img in imgs:
                    if('AdvCash' in img):
                        takes.append('AdvCash')
                    if('PerfectMoney' in img):
                        takes.append('PerfectMoney')
                    if('Payeer' in img):
                        takes.append('Payeer')
                    if('Bitcoin' in img):
                        takes.append('Bitcoin')
                item['takes'] = takes
                item['button_myinvestblog'] = self.get_code('http://myinvestblog.ru/' + id.replace('go','getdata&do=button'))
                resp = requests.get('http://myinvestblog.ru/'+id.replace('go','rcb'))
                item['RCB_myinvestblog'] = self.get_RCB_myinvestblog(resp)
                try:
                    item['RCB_data_myinvestblog'] = self.get_RCB_data_myinvestblog(resp)
                except:
                    item['RCB_data_myinvestblog'] = ''


                item['investmentplan'] = div.css(' table:nth-child(3) > tr > td:nth-child(1) > table > tr > td:nth-child(2) > span::text').extract_first().strip()
                yield item
