# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider

class graspgold(BaseSpider):
    name = 'graspgold'
    allowed_domains = ['graspgold.com']
    start_urls = ['https://graspgold.com/home/']

    def start_requests(self):
        urls = [
            'https://graspgold.com/home/?lang=en',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.ins_wrap')
        for div in divs:
            item = {}
            id = div.css('div.d_name_l a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://graspgold.com/go/lid/' + id)
            item['name'] = div.css('div.d_name_l a::text').extract_first().strip()
            if (self.check_new(item['url'])):
                item['script'] = self.get_script(item['url'])
                self.generate_thumbnail(item['url'], item['name'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                item['status'] = div.css('div.status_vote span::text').extract_first().strip()
                item['status_graspgold'] = div.css('div.status_vote span::text').extract_first().strip()
                item['lastpaid'] = div.css('div.status_pay span::text').extract_first().strip()
                item['investmentplan'] = div.css('div.fcprogramplan').extract_first().strip().replace(
                    '<div class="fcprogramplan">', '').replace('<span>Investment Plan</span>', '').replace('</div>', '')
                info_trs = div.css('table.tg tbody tr')
                item['Min/Max'] = info_trs[0].css('td b::text').extract()[3]

                takes = []
                for img in info_trs[3].css('td')[0].css('img::attr(title)').extract():
                    takes.append(img)
                item['takes'] = takes

                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']

                item['withdrawtype'] = info_trs[0].css('td b::text').extract()[1].strip()
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                item['button_graspgold'] = self.get_code('https://graspgold.com/get_code/lid/' + id)[0].encode(
                    'utf-8')
                resp = requests.get('https://graspgold.com/refback/lid/' + id)
                item['RCB_graspgold'] = self.get_RCB_classformnosize(resp)
                item['RCB_data_graspgold'] = self.get_RCB_data_classlist(resp)

                yield item
