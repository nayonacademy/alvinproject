# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider

class graspgoldlean(BaseSpider):
    name = 'graspgoldlean'
    allowed_domains = ['graspgold.com']
    start_urls = ['https://graspgold.com/home/']

    def start_requests(self):
        urls = [
            'https://graspgold.com/home/?lang=en',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.ins_wrap')
        for div in divs:
            item = {}
            id = div.css('div.d_name_l a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://graspgold.com/go/lid/' + id)
            item['status'] = div.css('div.status_vote span::text').extract_first().strip()
            item['status_graspgold'] = div.css('div.status_vote span::text').extract_first().strip()
            item['lastpaid'] = div.css('div.status_pay span::text').extract_first().strip()
            item['button_graspgold'] = self.get_code('https://graspgold.com/get_code/lid/' + id)[0].encode(
                'utf-8')
            resp = requests.get('https://graspgold.com/refback/lid/' + id)
            item['RCB_graspgold'] = self.get_RCB_classformnosizze(resp)
            item['RCB_data_graspgold'] = self.get_RCB_data_classlist(resp)

        yield item


    def get_RCB(self, url):
        resp = requests.get(url)
        RCB = []
        response = BeautifulSoup(resp.text)
        table = response.find('table', {'class': 'form nosize'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        rcb['min'] = td_list[0].text
                        rcb['max'] = td_list[1].text
                        rcb['percentage'] = td_list[2].text
                        rcb['1stDepRCB'] = td_list[3].text
                        rcb['1stbonus'] = td_list[4].text
                        rcb['redep-rcb'] = td_list[5].text
                        rcb['redep-bonus'] = td_list[6].text
                        RCB.append(rcb)
                    except:
                        pass
        return RCB


    def get_RCB_data(self, resp):
        RCB = []
        response = BeautifulSoup(resp.text)
        table = response.find('table', {'class': 'table list'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        rcb['date'] = td_list[2].text
                        rcb['depo'] = td_list[3].text.replace('$', '')
                        # rcb['rcb'] = td_list[2].text.split('/')[1].replace('$','')
                        rcb['user'] = td_list[1].text
                        RCB.append(rcb)
                    except:
                        pass
        return RCB
