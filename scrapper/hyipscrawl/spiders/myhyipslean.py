# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class myhyipslean(BaseSpider):
    name = 'myhyipslean'
    allowed_domains = ['myhyips.net']
    start_urls = ['http://myhyips.net/home/']

    def start_requests(self):
        urls = [
            'https://myhyips.net/home/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.khunglon')
        for div in divs:
            item = {}
            id = div.css('div.nameprogram a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://myhyips.net/go/lid/'+id)
            item['status'] = div.css('div.khunggiua3 div.k-status span::text').extract_first().strip()
            item['status_myhyips'] = div.css('div.khunggiua3 div.k-status span::text').extract_first().strip()
            item['lastpaid'] = div.css('div.k-mod span.label-primary::text').extract_first().strip()
            item['button_hyipclub'] = self.get_code('https://myhyips.net/get_code/lid/' + id)[0].encode(
                'utf-8')

            resp = requests.get('https://myhyips.net/refback/lid/'+id)
            item['RCB_myhyips'] = self.get_RCB_myhyips(resp)
            item['RCB_data_myhyips'] = self.get_RCB_data_myhyips(resp)

            yield item
