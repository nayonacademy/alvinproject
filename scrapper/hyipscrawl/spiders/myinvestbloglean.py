# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class myinvestbloglean(BaseSpider):
    name = 'myinvestbloglean'
    allowed_domains = ['myinvestblog.ru']
    start_urls = ['http://myinvestblog.ru/?lang=en']

    def start_requests(self):
        urls = [
            'http://myinvestblog.ru/?lang=en',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # table = response.css('table')
        divs = response.xpath('//div[@style="max-width:780px;"]')
        for div in divs:
            item = {}
            id = div.css('a.list-aname::attr(href)').extract_first()
            item['url'] = self.get_url('http://myinvestblog.ru/' + id)
            tds_1 = div.css('div.list2 > table > tr > td')
            for tdx, td in enumerate(tds_1):
                if tdx == 1:
                    status_img = td.css('img::attr(src)').extract_first()
                if('_1' in status_img):
                    item['status'] = 'PAYING'
                elif('_2' in status_img):
                    item['status'] = 'WAITING'
                else:
                    item['status'] = 'PROBLEM'
                item['Min/Max'] = td.css('b::text').extract_first().strip()
                item['withdrawtype'] = td.css('b::text').extract()[3].strip()
                item['status_myinvestblog'] = item['status']

                resp = requests.get('http://myinvestblog.ru/'+id.replace('go','rcb'))
                item['RCB_myinvestblog'] = self.get_RCB_myinvestblog(resp)
                item['RCB_data_myinvestblog'] = self.get_RCB_data_myinvestblog(resp)
                yield item
