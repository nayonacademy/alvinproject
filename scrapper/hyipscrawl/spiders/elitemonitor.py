# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class elitemonitor(BaseSpider):
    name = 'elitemonitor'
    allowed_domains = ['elite-monitor.com']
    start_urls = ['https://elite-monitor.com/']

    def start_requests(self):
        urls = [
            'https://elite-monitor.com/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.khunglon')
        for div in divs:
        #try:
            item = {}
            id = div.css('div.nameprogram a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://elite-monitor.com/go/lid/'+id+'/')
            item['name'] = div.css('div.nameprogram a:not(span)::text').extract_first().strip() #item['url'].split('/')[2]#div.css('li.name a::text').extract_first().strip()
            if (self.check_new(item['url'])):
                item['script'] = self.get_script(item['url'])
                self.generate_thumbnail(item['url'], item['name'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                item['lastpaid'] = div.css('div.khunglon div.khunggiua div.khunggiua3 div.k-mod span.label::text').extract_first().strip()
                item['status'] = div.css('div.k-status span::text').extract_first().strip()
                item['status_elitemonitor'] = div.css('div.k-status span::text').extract_first().strip()
                item['investmentplan'] = div.css('div.khungplan p::text').extract_first().strip().encode('utf-8').decode('unicode_escape')
                item['Min/Max'] = div.css('div.khung-p4 span.min::text').extract_first().strip()+'/'+ div.css('div.khung-p4 span.max::text').extract_first().strip()
                item['withdrawtype'] = div.css('div.khung-p3 li:nth-of-type(3) span.mday::text').extract_first().strip()
                item['takes'] = div.css('div.khung-p3 li:nth-of-type(4) img::attr(title)').extract()
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']
                item['button_elitemonitor'] = self.get_code('https://elite-monitor.com/get_code/lid/'+id)[0].encode('utf-8')
                resp = requests.get('https://elite-monitor.com/refback/lid/'+id+'/')
                item['RCB_elitemonitor'] = self.get_RCB_elitemonitor(resp)
                item['RCB_data_elitemonitor'] = self.get_RCB_data_elitemonitor(resp)
                #except:
 