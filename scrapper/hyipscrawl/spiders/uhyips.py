# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class uhyips(BaseSpider):
    name = 'uhyips'
    allowed_domains = ['uhyips.com']
    start_urls = ['http://uhyips.com/']

    def start_requests(self):
        urls = [
            'http://uhyips.com/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        divs = response.css('div.listing')
        resp_soup = BeautifulSoup(response.text, features='lxml')
        divs = resp_soup.find_all('div',{'class':'listing'})
        for div in divs:
            item = {}
            id = div.find('table',{'class':'listinglefttable'}).find('tr',{'class':'prog_paying'}).find_all('td')[0].find('a').attrs['href'].strip().split('?')[1].replace('lid=','')
            item['url'] = self.get_url('https://uhyips.com/visit?lid='+id)
            print("Item URL :: ", item['url'])
            if (self.check_new(item['url'])):
                item['name'] = div.find('table',{'class':'listinglefttable'}).find('tr',{'class':'prog_paying'}).find_all('td')[0].find('a').text.strip()
                item['script'] = self.get_script(item['url'])
                self.generate_thumbnail(item['url'], item['name'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['status'] = div.find('table',{'class':'listinglefttable'}).find_all('tr')[0].find_all('td')[1].find('b').text.strip()
                item['status_uhyips'] = item['status']
                item['lastpaid'] = div.find('table',{'class':'listinglefttable'}).find('tr', {'class': 'prog_paying'}).find_all('td')[2].text.strip()
                item['investmentplan'] = div.find('table',{'class':'listinglefttable'}).find_all('td',{'colspan':'2'})[0].text.replace('investment plans:','').encode('utf-8').decode('unicode_escape')
                item['Min/Max'] = div.find('table',{'class':'listinglefttable'}).find_all('tr')[1].find_all('td',{'valign':'top'})[1].find_all('b')[0].text

                item['withdrawtype'] = div.find('table',{'class':'listinglefttable'}).find_all('tr')[1].find_all('td',{'valign':'top'})[1].find_all('b')[1].text
                takes = []
                takes_soup = div.find('table',{'class':'listinglefttable'}).find_all('tr')[3].find_all('td')[0].find_all('a')
                for take in takes_soup:
                    takes.append(take.find('img').attrs['alt'])
                item['takes'] = takes
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                print("created datea :: ",self.get_created_date(item['url']))
                item['forums'] = self.get_forums(item['url'])
                item['button_uhyips'] = self.get_code('https://uhyips.com/statusbutton?lid=' + id)[0].encode(
                        'utf-8')
                rcb_request = requests.get('http://uhyips.com/rcbrequest?lid='+id)
                item['RCB_uhyips'] = self.get_RCB_uhyips(rcb_request)
                item['RCB_data_uhyips'] = self.get_RCB_data_uhyips(rcb_request)
                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                if('issuerbrand' in obj):
                    item['issuerbrand'] = obj['issuerbrand'][0]
                if('validation_type' in obj):
                    item['validation_type'] = obj['validation_type']
                #except:
                    #pass
                yield item


