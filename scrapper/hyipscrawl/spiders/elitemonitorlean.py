# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class elitemonitorlean(BaseSpider):
    name = 'elitemonitorlean'
    allowed_domains = ['elite-monitor.com']
    start_urls = ['https://elite-monitor.com/']

    def start_requests(self):
        urls = [
            'https://elite-monitor.com/',
            'https://elite-monitor.com/home/type/6/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.khunglon')
        for div in divs:
        #try:
            item = {}
            id = div.css('div.nameprogram a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://elite-monitor.com/go/lid/'+id+'/')
            item['lastpaid'] = div.css('div.khunglon div.khunggiua div.khunggiua3 div.k-mod span.label::text').extract_first().strip()
            item['status'] = div.css('div.k-status span::text').extract_first().strip()
            item['status_elitemonitor'] = div.css('div.k-status span::text').extract_first().strip()
            item['button_elitemonitor'] = self.get_code('https://elite-monitor.com/get_code/lid/' + id)[0].encode('utf-8')

            resp = requests.get('https://elite-monitor.com/refback/lid/'+id+'/')
            item['RCB_elitemonitor'] = self.get_RCB_elitemonitor(resp)
            item['RCB_data_elitemonitor'] = self.get_RCB_data_elitemonitor(resp)
            #except:
             #   pass
            yield item
