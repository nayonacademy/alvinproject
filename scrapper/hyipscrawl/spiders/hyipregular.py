# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider

class hyipregular(BaseSpider):
    name = 'hyipregular'
    allowed_domains = ['hyipregular.org']
    start_urls = ['http://hyipregular.org/']

    def start_requests(self):
        urls = [
            'http://hyipregular.org/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        resp_soup = BeautifulSoup(response.text, features='lxml')
        print(resp_soup)
        divs = response.xpath('//div[@style="padding: 6px; border: 2px groove #1C86EE; word-wrap: break-word;"]')
        for div in divs:
            item = {}
            id = div.xpath('//font[@size="2"]').css('a::attr(href)').extract_first().replace('?a=go&lid=','')
            item['url'] = self.get_url('http://hyipregular.org/?a=go&lid=' + id)
            item['name'] = div.xpath('//font[@size="2"]').css('a::text').extract_first().strip()
            img = div.css('table').css('tr')[1].css('td')[0].css('img::attr(src)').extract_first()
            if img == 'images/h_1.gif':
                item['status'] = 'Paying'
            else:
                item['status'] = 'Waiting'

            item['status_hyipregular'] = item['status']
            self.generate_thumbnail(item['url'], item['name'])
            item['script'] = self.get_script(item['url'])
            domain_details = self.get_domain(item['url'])
            if ('datacenter' in domain_details):
                item['datacenter'] = domain_details['datacenter']
                item['ip'] = domain_details['ip']
                item['servers'] = domain_details['servers']
            item['forums'] = self.get_forums(item['url'])
            item['investmentplan'] = div.css('table').css('tr')[1].css('td::text')[2].extract()

            print(item)
            return


            item['investmentplan'] = div.find('div', {'class': 'percents'}).text
            item['lastpaid'] = div.find('div', {'class': 'lastpayout'}).find('b').text
            item['Min/Max'] = div.find('div', {'class': 'amountdiv'}).find('b',
                                                                           {'class': 'min'}).text + ' / ' + div.find(
                'div', {'class': 'amountdiv'}).find('b', {'class': 'max'}).text
            item['withdrawtype'] = div.find('div', {'class': 'withdrawal'}).find('b').text
            takes = []
            for img in div.find('div', {'class': 'paysystems'}).find_all('img'):
                takes.append(img.attrs['alt'])
            item['takes'] = takes
            obj1 = self.get_buttons_and_statuses(item['url'])
            item['buttons'] = obj1['buttons']
            item['status1'] = obj1['status1']
            item['status2'] = obj1['status2']
            item['created_date'] = self.get_created_date(item['url'])
            item['button_allhyips'] = self.get_code('https://all-hyips.info/get_code/lid/' + id)[0].encode(
                'utf-8')
            resp = requests.get('https://all-hyips.info/refback/lid/' + id + '/')
            item['RCB_allhyips'] = self.get_RCB_classformnosize(resp)
            item['RCB_data_allhyips'] = self.get_RCB_data_classlist(resp)
            obj = self.scrape_ssl(item['url'])
            if('valid' in obj):
                item['valid'] = obj['valid']
                item['issuerbrand'] = obj['issuerbrand'][0]
                item['validation_type'] = obj['validation_type']
        # except:
        #   pass
            yield item

