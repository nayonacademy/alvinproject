# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class hyipcruiser(BaseSpider):
    name = 'hyipcruiser'
    allowed_domains = ['hyip-cruiser']
    start_urls = ['http://hyip-cruiser.com/']

    def start_requests(self):
        urls = [
            'http://hyip-cruiser.com/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        resp_soup = BeautifulSoup(response.text, features='lxml')
        divs = resp_soup.find_all('div', {'class': 'program'})
        for div in divs:
            item = {}
            id = div.find('div', {'class': 'title'}).find('a').attrs['href'].split('/')[5]
            item['url'] = self.get_url('http://hyip-cruiser.com/go/lid/' + id)
            item['name'] = div.find('div', {'class': 'title'}).find('a').text.strip()  # item['url'].split('/')[2]
            if (self.check_new(item['url'])):
                self.generate_thumbnail(item['url'], item['name'])
                item['script'] = self.get_script(item['url'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                classes_stat = div.find('div', {'class': 'status'}).attrs['class']
                for classe_stat in classes_stat:
                    if ('status1' in classe_stat):
                        item['status'] = 'Paying'
                    if ('status2' in classe_stat):
                        item['status'] = 'Waiting'
                item['status_hyipcruiser'] = item[
                        'status']  # div.find('div', {'class': 'status'}).xpath("@class").extract()
                item['investmentplan'] = div.find('div', {'class': 'percents'}).text
                item['lastpaid'] = div.find('div', {'class': 'lastpayout'}).find('b').text
                item['Min/Max'] = div.find('div', {'class': 'amountdiv'}).find('b',
                                                                                   {'class': 'min'}).text + ' / ' + div.find(
                        'div', {'class': 'amountdiv'}).find('b', {'class': 'max'}).text
                item['withdrawtype'] = div.find('div', {'class': 'withdrawal'}).find('b').text
                takes = []
                for img in div.find('div', {'class': 'paysystems'}).find_all('img'):
                    takes.append(img.attrs['src'].replace('/images/', '').replace('.png', ''))
                item['takes'] = takes
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                resp = requests.get('http://hyip-cruiser.com/?a=refback&lid=' + id + '/')
                item['RCB_hyipcruiser'] = self.get_RCB_classformnosize(resp)
                item['RCB_data_hyipcruiser'] = self.get_RCB_data_classlist(resp)
                item['button_hyipcruiser'] = self.get_code('https://hyip-cruiser.com/get_code/lid/' + id)[0].encode(
                        'utf-8')
                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']
                yield item



