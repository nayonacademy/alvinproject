# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider

class allhyips(BaseSpider):
    name = 'allhyips'
    allowed_domains = ['all-hyips.info']
    start_urls = ['https://all-hyips.info/']
    custom_settings = {
        'DOWNLOAD_DELAY': '90',
    }

    def start_requests(self):
        urls = [
            'https://all-hyips.info/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        resp_soup = BeautifulSoup(response.text, features='lxml')
        divs = resp_soup.find_all('div', {'class': 'program'})
        for div in divs:
            item = {}
            # try:
            id = div.find('div', {'class': 'title'}).find('a').attrs['href'].split('/')[5]
            item['url'] = self.get_url('https://all-hyips.info/go/lid/' + id)
            item['name'] = div.find('div', {'class': 'title'}).find('a').text.strip() #item['url'].split('/')[2]
            if (self.check_new(item['url'])):
                self.generate_thumbnail(item['url'],item['name'])
                item['script'] = self.get_script(item['url'])
                domain_details = self.get_domain(item['url'])
                if('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                item['status'] = div.find('div', {'class': 'status'}).text
                item['status_allhyips'] = div.find('div', {'class': 'status'}).text
                item['investmentplan'] = div.find('div', {'class': 'percents'}).text
                item['lastpaid'] = div.find('div', {'class': 'lastpayout'}).find('b').text
                item['Min/Max'] = div.find('div', {'class': 'amountdiv'}).find('b',
                                                                               {'class': 'min'}).text + ' / ' + div.find(
                    'div', {'class': 'amountdiv'}).find('b', {'class': 'max'}).text
                item['withdrawtype'] = div.find('div', {'class': 'withdrawal'}).find('b').text
                takes = []
                for img in div.find('div', {'class': 'paysystems'}).find_all('img'):
                    takes.append(img.attrs['alt'])
                item['takes'] = takes
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                item['button_allhyips'] = self.get_code('https://all-hyips.info/get_code/lid/' + id)[0].encode(
                    'utf-8')
                resp = requests.get('https://all-hyips.info/refback/lid/' + id + '/')
                item['RCB_allhyips'] = self.get_RCB_classformnosize(resp)
                item['RCB_data_allhyips'] = self.get_RCB_data_classlist(resp)
                obj = self.scrape_ssl(item['url'])
                if('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']
            # except:
            #   pass
                yield item

