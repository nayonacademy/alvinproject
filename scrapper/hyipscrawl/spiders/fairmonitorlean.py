# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider



class fairmonitorlean(BaseSpider):
    name = 'fairmonitorlean'
    allowed_domains = ['fairmonitor.com']
    start_urls = ['https://fairmonitor.com/?lang=fr']

    def start_requests(self):
        urls = [
            'https://fairmonitor.com/?lang=fr',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.prog-folder')
        for div in divs:
            item = {}
            # try:
            id = div.css('li.name a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://fairmonitor.com/go/lid/' + id)
            item['status'] = div.css('li.status span::text').extract_first().strip()
            item['status_fairmonitor'] = div.css('li.status span::text').extract_first().strip()
            item['lastpaid'] = div.css('li.last::text').extract_first().strip().strip()
            item['button_fairmonitor'] = self.get_code('https://fairmonitor.com/get_code/lid/' + id)[0].encode(
                'utf-8')
            resp = requests.get('https://fairmonitor.com/refback/lid/' + id + '/?lang=fr')
            item['RCB_fairmonitor'] = self.get_RCB_classformnosize(resp)
            item['RCB_data_fairmonitor'] = self.get_RCB_data_classlist(resp)(resp)
            # except:
        #     pass

            yield item

    def get_RCB(self, resp):
        RCB = []
        response = BeautifulSoup(resp.text, features='lxml')
        table = response.find('table', {'class': 'nosize'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    #try:
                    rcb['min'] = td_list[0].text
                    rcb['max'] = td_list[1].text
                    rcb['percentage'] = td_list[2].text
                    rcb['1stDepRCB'] = td_list[3].text
                    rcb['1stbonus'] = td_list[4].text
                    rcb['redep-rcb'] = td_list[5].text
                    rcb['redep-bonus'] = td_list[6].text
                    RCB.append(dict(rcb))
                    #except:
                        #pass
        return RCB

    def get_RCB_data(self, resp):
        RCB_data = []
        response = BeautifulSoup(resp.text, features='lxml')
        table = response.find('table', {'class': 'list'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    #try:
                    rcb['date'] = td_list[0].text
                    rcb['depo'] = td_list[2].text.split('/')[0].replace('$', '')
                        # rcb['rcb'] = td_list[2].text.split('/')[1].replace('$','')
                    rcb['user'] = td_list[1].text
                    RCB_data.append(dict(rcb))
                    #except:
                     #   pass
        return RCB_data
