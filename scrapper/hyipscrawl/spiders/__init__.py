# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

import datetime
from bs4 import BeautifulSoup
import requests
import scrapy
import json
import urllib3
import shutil
import pymongo
urllib3.disable_warnings()


class BaseSpider(scrapy.Spider):

    def get_buttons_and_statuses(self ,url):
        name = self.getname(url)
        obj = {}
        resp = requests.get('http://www.allhyipmonitors.com/details/' + name)
        buttons = []
        response = BeautifulSoup(resp.text, features = 'lxml')
        div = response.find('div' ,{'class' :'mtd_wrap'})
        if(div is not None):
            divs = div.find_all('div' ,{'class' : 'mtd'})
            for div in divs:
                button = div.find('a' ,{'class' :'mbutton'})
                buttons.append(button.encode('utf-8').decode('unicode_escape'))
        obj['buttons'] = buttons
        try:
            obj['status1'] = response.find('div', {'class' : 'status1'}).text
        except:
            obj['status1'] = ''
        try:
            obj['status2'] = response.find('div',{'class' : 'status2'}).text
        except:
            obj['status2'] = ''
        return obj

    def get_created_date(self ,url):
        name = self.getname(url)
        resp = requests.get('http://allhyipmon.ru/en/monitor/' + name)
        response = BeautifulSoup(resp.text, features='lxml')
        if (response.find('table', {'class': 'pshow'}) is not None):
            print(response.find('table', {'class': 'pshow'}).find_all('table')[2].find_all('tr')[1].find_all('td')[2].text)
            created_date = \
            response.find('table', {'class': 'pshow'}).find_all('table')[2].find_all('tr')[1].find_all('td')[
                2].text.split(' ')[0].split('/')
            print(created_date)
            return datetime.datetime(int(created_date[2]), int(created_date[1]), int(created_date[0]))
        else:
            return 'N/A'

    def get_url(self,url):
        resp = requests.get(url, allow_redirects=False)
        if ('Location' in resp.headers):
            print(resp.headers['Location'])
            url =  resp.headers['Location'].split('?')[0]
            url_arr = url.split('/')
            url =  url_arr[2]
            print(url)
            return url
        else:
            resp = requests.get(url)
            print(resp.url)
            url_arr = resp.url.split('/')
            url =  url_arr[2]
            print(url)
            return url

    def get_code(self, url):
        resp = requests.get(url, allow_redirects=False)
        print(url)
        response = BeautifulSoup(resp.text, features='lxml')
        code = response.find('textarea').contents
        return code

    def scrape_ssl(self,url):
        url = 'https://ssltools.digicert.com/chainTester/webservice/cert/info?domain='+url+'&port=443&format=json'
        resp = requests.get(url)
        obj = {}
        json_data = json.loads(resp.text)
        try:
            certificate = json_data['data']['certAlgList'][0]['certList'][0]
            obj['valid'] = certificate['validTo']
            obj['issuerbrand'] = certificate['issuedByOrg']
            obj['validation_type'] = certificate['productType']
            print(obj)
        except:
            pass
        return obj
    def scrape_ssl_1(self, url):

        payload = {'domain': url, 'port': '443'}
        obj = {}

        resp = requests.post('https://cryptoreport.geotrust.com/chainTester/webservice/validatecerts/json',payload)
        json_data =  json.loads(resp.text)
        try:
            cert_list = json_data['certAlgList'][0]['certList'][0]

            obj['valid'] =  cert_list['validTo'] #table.find_all('tr')[7].find('td').find('span',{'class' : 'comment'}).text
            obj['issuerbrand'] = cert_list['issuedByOrg']#table.find_all('tr')[10].find_all('td')[0].text
            obj['validation_type'] = cert_list['productType']#table.find_all('tr')[11].find('td').find('span',{'class' : 'bad'}).text
        except:
            pass
        return obj

    def get_script(self,url):
        try:
            try:
                resp = requests.get(url + '?a=signup', verify=False)
            except:
                resp.status_code = 403
            if (resp.status_code == 200):
                return 'Goldcoder Script'
            try:
                resp = requests.get(url + 'signup', verify=False)
            except:
                resp.status_code = 403
            if (resp.status_code == 200):
                return 'Blitz Script'
            try:
                resp = requests.get(url + 'register', verify=False)
            except:
                resp.status_code = 403
            if (resp.status_code == 200):
                return 'DQ Script'
            try:
                resp = requests.get(url + 'client/register', verify=False)
            except:
                resp.status_code = 403
            if (resp.status_code == 200):
                return 'echyip Script'
            try:
                resp = requests.get(url + 'client/registration/', verify=False)
            except:
                resp.status_code = 403
            if (resp.status_code == 200):
                return 'AS Script'
            try:
                resp = requests.get(url + 'client/registration', verify=False)
            except:
                resp.status_code = 403
            if (resp.status_code == 200):
                return 'H-Script'
        except:
            pass

    def get_domain(self,url):
        #url_arr = url.split('/')
        domain = url
        if(len(domain.split('.')) > 2):
            domain = domain.split('.')[1]+'.'+domain.split('.')[2]
        payload = {'domain':domain , 'action':'wx__domain_hostcheker'}
        resp = requests.post('https://www.hostingchecker.com/wp-admin/admin-ajax.php',data= payload)
        response = BeautifulSoup(resp.text, features= 'lxml')
        ul = response.find('ul',{'class' : 'find_info'})
        obj = {}
        if(ul is  None):
            return {}
        ul = ul.find_all('li')
        if len(ul) > 0:

            obj['datacenter'] = ul[0].find('span').text
            obj['ip'] = ul[1].find('span').text
            try:
                obj['servers'] = ul[5].find('span').text
            except:
                obj['servers'] = 'n/a'
        return obj

    def get_forums(self,url):
        domain = self.getname(url)
        resp = requests.get('http://allhyipmon.ru/en/monitor/'+domain)
        response = BeautifulSoup(resp.text, features='lxml')
        monitors_span = response.find_all('span',{'class' : 'img_monitor2'})
        monitors = []
        for span in monitors_span:
            link = span.find('a').attrs['href']
            monitors.append(link)
        return monitors

    def generate_thumbnail(self,url,name):
        r = requests.get("https://blinky.nemui.org/show?uri="+url)
        r = requests.get('https://blinky.nemui.org/shot?'+url,stream = True )
        with open('images/'+name.replace('.','_') , 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)

    def getname(self,url):

        domain = url
        if (len(domain.split('.')) > 2):
            domain = domain.split('.')[1] + '.' + domain.split('.')[2]
        return domain

    def get_RCB_classformnosize(self, resp):
        RCB = []
        response = BeautifulSoup(resp.text, features='lxml')
        table = response.find('table', {'class': 'form nosize'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        rcb['min'] = td_list[0].text
                        rcb['max'] = td_list[1].text
                        rcb['percentage'] = td_list[2].text
                        rcb['1stDepRCB'] = td_list[3].text
                        rcb['1stbonus'] = td_list[4].text
                        rcb['redep-rcb'] = td_list[5].text
                        rcb['redep-bonus'] = td_list[6].text
                        RCB.append(dict(rcb))
                    except:
                        pass
        return RCB

    def get_RCB_data_classlist(self, resp):
        RCB_data = []
        response = BeautifulSoup(resp.text, features='lxml')
        table = response.find('table', {'class': 'list'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        status = td_list[4].text
                        if ('Paid' in status):
                            rcb['date'] = td_list[0].text
                            rcb['depo'] = td_list[2].text.split('/')[0].replace('$', '')
                            rcb['user'] = td_list[1].text
                            RCB_data.append(dict(rcb))
                    except:
                        pass
        return RCB_data


    def get_RCB_elitemonitor(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text, features = 'lxml')
        try:
            table = response.find_all('table',{'class' : 'list-grid'})[1]
        except:
            table = None
        if (table is not None):
            for tr in table.find_all('tr'):
                td_list = tr.find_all("td")
                rcb = {}
                if len(td_list) > 0:
                    try:
                        rcb['min'] = td_list[0].text
                        rcb['max'] = td_list[1].text
                        rcb['percentage'] = td_list[2].text
                        rcb['1stDepRCB'] = td_list[3].text
                        rcb['1stbonus'] = td_list[4].text
                        rcb['redep-rcb'] = td_list[5].text
                        rcb['redep-bonus'] = td_list[6].text
                        RCB.append(rcb)
                    except:
                        pass
        return RCB

    def get_RCB_data_elitemonitor(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text , features = 'lxml')
        table = response.find('table',{'class' : 'table list-grid'})
        if(table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        status = td_list[5].text
                        if('Paid' in status):
                            rcb['date'] = td_list[1].text
                            rcb['depo'] = td_list[3].text.split('/')[0].replace('$','').replace(',', '')
                            #rcb['rcb'] = td_list[2].text.split('/')[1].replace('$','')
                            rcb['user'] = td_list[2].text
                            RCB.append(rcb)
                    except:
                        pass
        return RCB

    def get_RCB_investtracing(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text, features = 'lxml')
        table = response.find('table',{'class' : 'table table-striped table-bordered table-hover table-condensed'})
        if (table is not None):
            for tr in table.find_all('tr',{'class' : 'rcbtd success'}):
                rcb = {}
                td_list = tr.find_all("td")
                rcb['min'] = td_list[0].text.split('-')[0].replace('$','')
                rcb['max'] = td_list[0].text.split('-')[1]
                rcb['percentage'] = td_list[1].text
                t1 = td_list[2].text.split('(')[0]
                print(t1)
                rcb['1stDepRCB'] = t1.split('+')[0]
                if(len(t1.split('+')) > 1):
                    rcb['1stbonus'] = t1.split('+')[1]
                else:
                    rcb['1stbonus'] = 0
                t2 = td_list[3].text.split('(')[0]
                print (t2)
                rcb['redep-rcb'] = t2.split('+')[0]
                if (len(t2.split('+')) > 1):
                    rcb['redep-bonus'] = t2.split('+')[1]
                else:
                    rcb['redep-bonus'] = 0
                RCB.append(rcb)
        return RCB

    def get_RCB_data_investtracing(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text, features = 'lxml')
        table = response.find('table',{'class':'table table-striped table-bordered table-hover table-condensed rcbrecords'})
        if(table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    status = td_list[5].text
                    if ('paid' in status):
                        rcb['date'] = td_list[0].text
                        rcb['depo'] = td_list[3].text.split('/')[0].replace('$','')
                        rcb['user'] = td_list[2].text
                        RCB.append(rcb)

        return RCB

    def get_RCB_myhyips(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text, features='lxml')
        table = response.find('table',{'class' : 'list-grid'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    rcb['min'] = td_list[0].text
                    rcb['max'] = td_list[1].text
                    rcb['percentage'] = td_list[2].text
                    rcb['1stDepRCB'] = td_list[3].text
                    rcb['1stbonus'] = td_list[4].text
                    rcb['redep-rcb'] = td_list[5].text
                    rcb['redep-bonus'] = td_list[6].text
                    RCB.append(rcb)
        return RCB

    def get_RCB_data_myhyips(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text, features='lxml')
        table = response.find('table',{'class':'table list-grid'})
        if(table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    status = td_list[4].text
                    if ('Paid' in status):
                        rcb['date'] = td_list[0].text
                        rcb['depo'] = td_list[2].text.split('/')[0].replace('$','')
                        rcb['user'] = td_list[1].text
                        RCB.append(rcb)
        return RCB

    def get_RCB_uhyips(self, resp):
        RCB = []
        response = BeautifulSoup(resp.text, features='lxml')
        if(response.find('div', {'class': 'rcbtable'}) is not None):
            div = response.find_all('div', {'class': 'rcbtable'})[0]
            table = div.find('table', {'class': 'tableborder'})
            if (table is not None):
                for tr in table.find_all('tr'):
                    rcb = {}
                    td_list = tr.find_all("td")
                    if len(td_list) > 2:
                        rcb['min'] = td_list[0].text.split('-')[0].replace('$', '')
                        rcb['max'] = td_list[0].text.split('-')[1].replace('$', '')
                        rcb['percentage'] = td_list[1].text
                        rcb['1stDepRCB'] = td_list[2].text.split('+')[0]
                        if (len(td_list[2].text.split('+')) > 1):
                            rcb['1stbonus'] = td_list[2].text.split('+')[1]
                        else:
                            rcb['1stbonus'] = 0
                        if(len(td_list) > 3):
                            rcb['redep-rcb'] = td_list[3].text.split('+')[0]
                            if (len(td_list[3].text.split('+')) > 1):
                                rcb['redep-bonus'] = td_list[3].text.split('+')[1]
                            else:
                                rcb['redep-bonus'] = 0
                        else:
                            rcb['redep-rcb'] = '100%'
                            rcb['redep-bonus'] = 0
                        RCB.append(rcb)

        return RCB

    def get_RCB_data_uhyips(self, resp):
        RCB = []
        response = BeautifulSoup(resp.text, features='lxml')
        if(len(response.find_all('div', {'class': 'rcbtable'})) < 2):
            return RCB
        div = response.find_all('div', {'class': 'rcbtable'})[1]
        table = div.find('table', {'class': 'tableborder'})

        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        status = td_list[6].text
                        if ('Paid' in status):
                            rcb['date'] = td_list[0].text
                            rcb['depo'] = td_list[3].text.replace('$', '')
                            rcb['user'] = td_list[2].text
                            RCB.append(rcb)
                    except:
                        pass
        return RCB

    def get_RCB_myinvestblog(self, resp):
        RCB = []
        response = BeautifulSoup(resp.text, features='lxml')
        if(len(response.find_all('table', {'class': 'listbody'})) < 2):
            return RCB
        table = response.find_all('table', {'class': 'listbody'})[1]
        for tr in table.find_all('tr'):
            rcb = {}
            td_list = tr.find_all("td")
            if len(td_list) > 0:
                try:
                    rcb['min'] = td_list[0].text.split('-')[0].replace('$', '')
                    rcb['max'] = td_list[0].text.split('-')[1].replace('$', '')
                    rcb['percentage'] = td_list[1].text
                    rcb['1stDepRCB'] = td_list[2].text
                    if (td_list[3].text != '---'):
                        rcb['1stbonus'] = td_list[3].text
                    else:
                        rcb['1stbonus'] = 0
                    rcb['redep-rcb'] = td_list[4].text
                    if (td_list[5].text != '---'):
                        rcb['redep-bonus'] = td_list[5].text
                    else:
                        rcb['redep-bonus'] = 0
                    RCB.append(rcb)
                except:
                    pass
        return RCB
    def get_RCB_data_myinvestblog(selfself,resp):
        RCB_data = []
        response = BeautifulSoup(resp.text, features='lxml')
        index = 2
        if(len(response.find_all('table', {'class': 'listbody'})) < 3):
            index = len(response.find_all('table', {'class': 'listbody'})) - 1
        table = response.find_all('table', {'class': 'listbody'})[index]
        idx = 0
        for tr in table.find_all('tr'):
            rcb = {}
            if idx > 0:
                td_list = tr.find_all("td",{'bgcolor':'#FFFFFF'})
                if len(td_list) > 0 :
                    try:
                        status = td_list[4].text
                        if ('Paid' in status):
                            rcb['date'] = td_list[0].text
                            rcb['depo'] = td_list[2].text.split('/')[0].replace('$', '')
                            rcb['user'] = td_list[1].text
                            RCB_data.append(dict(rcb))
                    except:
                        pass
            idx = idx + 1
        return RCB_data

    def setClient(self):
        #mongo_uri = 'mongodb://127.0.0.1:27017/'
        self.collection_name = 'scrapper'
        # mongo_db = 'hyip_1'
        # self.client = pymongo.MongoClient(mongo_uri)
        # self.db = self.client[mongo_db]
    def closeClient(self):
        self.client.close()
    def check_new(self,url):
        exist = self.db[self.collection_name].find({'url': url}).count()
        if exist == 0:
            return True
        else:
            return False
