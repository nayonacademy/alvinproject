# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class myhyips(BaseSpider):
    name = 'myhyips'
    allowed_domains = ['myhyips.net']
    start_urls = ['http://myhyips.net/home/']

    def start_requests(self):
        urls = [
            'https://myhyips.net/home/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.khunglon')
        for div in divs:
            item = {}
            id = div.css('div.nameprogram a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://myhyips.net/go/lid/'+id)
            item['name'] = div.css('div.nameprogram a::text').extract_first().strip()
            if (self.check_new(item['url'])):
                item['script'] = self.get_script(item['url'])
                self.generate_thumbnail(item['url'], item['name'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                item['status'] = div.css('div.khunggiua3 div.k-status span::text').extract_first().strip()
                item['status_myhyips'] = div.css('div.khunggiua3 div.k-status span::text').extract_first().strip()
                item['lastpaid'] = div.css('div.k-mod span.label-primary::text').extract_first().strip()
                item['investmentplan'] = div.css('div.khungplan p::text').extract_first().strip().encode('utf-8').decode('unicode_escape')
                item['Min/Max'] = div.css('div.khunggiua4 > div.khung-p4 > ul > li:nth-of-type(1) > span::text').extract_first()
                item['withdrawtype'] = div.css('div.khunggiua4 > div.khung-p4 > ul > li:nth-of-type(3) > span::text').extract_first().strip()
                item['takes'] =  div.css(' div.khunggiua2 > div.khung-p3 > ul > li:nth-child(4) > span img::attr(title)').extract()
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                item['button_hyipclub'] = self.get_code('https://myhyips.net/get_code/lid/' + id)[0].encode(
                    'utf-8')
                resp = requests.get('https://myhyips.net/refback/lid/'+id)
                item['RCB_myhyips'] = self.get_RCB_myhyips(resp)
                item['RCB_data_myhyips'] = self.get_RCB_data_myhyips(resp)
                obj = self.scrape_ssl(item['url'])

                if('valid' in obj):
                    item['valid'] = obj['valid']
                if('issuerbrand' in obj):
                    item['issuerbrand'] = obj['issuerbrand'][0]
                if('validation_type' in obj):
                    item['validation_type'] = obj['validation_type']

                yield item
