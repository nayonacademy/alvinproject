# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class investtracinglean(BaseSpider):
    name = 'investtracinglean'
    allowed_domains = ['invest-tracing.com']
    start_urls = ['http://invest-tracing.com']

    def start_requests(self):
        urls = [
            'http://invest-tracing.com',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.listcontainer')
        for div in divs:
            item = {}
            id = div.css('div.listcontainer div.row div.text-center a::attr(href)').extract_first().strip().replace('program-','')
            item['url'] = self.get_url('http://invest-tracing.com/program-' + id)

            resp1 = requests.get('http://invest-tracing.com/detail-'+id)
            detailedresponse = BeautifulSoup(resp1.text)
            if(detailedresponse.find('label',{'class' : 'label label-success'})):
                item['status'] = 'Paying'
            elif(detailedresponse.find('label', {'class': 'label label-danger'})):
                item['status'] = 'Not Paying'
            else:
                item['status'] = 'Waiting'
            item['status_investtracing'] = item['status']
            divs = detailedresponse.find_all('div',{'class' : 'col-sm-3 col-xs-6'})
            item['lastpaid'] = divs[1].find_all('br')[2].nextSibling.strip()

            button_resp = requests.get('http://invest-tracing.com/detail-' + id)
            button_response = BeautifulSoup(button_resp.text, features='lxml')
            item['button_investtracing'] = button_response.find('div', {'class': 'col-md-8 col-sm-12'}).find(
                    'code').text.encode(
                    'utf-8')
            resp = requests.get('http://invest-tracing.com/rcb-'+id)
            item['RCB_investtracing'] = self.get_RCB_investtracing(resp)
            item['RCB_data_investtracing'] = self.get_RCB_data_investtracing(resp)


            yield item
