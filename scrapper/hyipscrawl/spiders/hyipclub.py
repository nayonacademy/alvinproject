# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class hyipclub(BaseSpider):
    name = 'hyipclub'
    allowed_domains = ['hyipclub.club']
    start_urls = ['http://hyipclub.club/']

    def start_requests(self):
        urls = [
            'http://hyipclub.club/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.khunglon')
        for div in divs:
            item = {}
            id = div.css('div.nameprogram a::attr(href)').extract_first().strip().split('/')[5]
            item['url'] = self.get_url('https://hyipclub.club/go/lid/'+id)
            item['name'] =  div.css('div.nameprogram a::text').extract_first().strip()
            if (self.check_new(item['url'])):
                item['script'] = self.get_script(item['url'])
                self.generate_thumbnail(item['url'], item['name'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['forums'] = self.get_forums(item['url'])
                item['status'] = div.css('div.k-status span::text').extract_first().strip()
                item['status_hyipclub'] = div.css('div.k-status span::text').extract_first().strip()
                item['lastpaid'] = 'n/a'#div.css('div.khunggia3 div.k-mod span.label::text').extract_first().strip()
                item['investmentplan'] = div.css('div.khungplan p::text').extract_first().strip().encode('utf-8').decode('unicode_escape')
                item['Min/Max'] = div.css('div.khung-p4 span.mday::text').extract_first().strip()
                item['withdrawtype'] = div.css('div.khung-p4 li:nth-of-type(3) span span.mday::text').extract_first().strip()
                item['takes'] = div.css('div.khungplan1 p img::attr(alt)').extract()
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                item['button_hyipclub'] = self.get_code('https://hyipclub.club/get_code/lid/' + id)[0].encode(
                    'utf-8')
                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']
                resp = requests.get('https://hyipclub.club/refback/lid/'+id)
                item['RCB_hyipclub'] = self.get_RCB_classformnosize(resp)
                item['RCB_data_hyipclub'] = self.get_RCB_data_classlist(resp)
                yield item
