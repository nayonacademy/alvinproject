# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class uhyipslean(BaseSpider):
    name = 'uhyipslean'
    allowed_domains = ['uhyips.com']
    start_urls = ['http://uhyips.com/']

    def start_requests(self):
        urls = [
            'http://uhyips.com/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.listing')
        resp_soup = BeautifulSoup(response.text, features='lxml')
        divs = resp_soup.find_all('div',{'class':'listing'})
        for div in divs:
            item = {}
           # try:
            id = \
                div.find('table', {'class': 'listinglefttable'}).find('tr', {'class': 'prog_paying'}).find_all('td')[
                    0].find('a').attrs['href'].strip().split('?')[1].replace('lid=', '')
                # id = div.css('table.listinglefttable > tbody  > tr.prog_paying >  td:nth-child(1) > span >  a::attr(href)').extract_first().strip().split('?')[1].replace('lid=','')
            item['url'] = self.get_url('https://uhyips.com/visit?lid=' + id)

            item['status'] = \
                div.find('table', {'class': 'listinglefttable'}).find_all('tr')[0].find_all('td')[
                    1].find('b').text.strip()
            item['status_uhyips'] = item['status']

            item['lastpaid'] = \
                div.find('table', {'class': 'listinglefttable'}).find('tr', {'class': 'prog_paying'}).find_all('td')[
                    2].text.strip()

            item['button_uhyips'] = self.get_code('https://uhyips.com/statusbutton?lid=' + id)[0].encode(
                    'utf-8')

            rcb_request = requests.get('http://uhyips.com/rcbrequest?lid='+id)
            item['RCB_uhyips'] = self.get_RCB_uhyips(rcb_request)
            item['RCB_data_uhyips'] = self.get_RCB_data_uhyips(rcb_request)
            #except:
             #   pass

            yield item


