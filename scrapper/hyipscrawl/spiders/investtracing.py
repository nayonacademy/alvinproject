# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class investtracing(BaseSpider):
    name = 'investtracing'
    allowed_domains = ['invest-tracing.com']
    start_urls = ['http://invest-tracing.com']

    def start_requests(self):
        urls = [
            'http://invest-tracing.com',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        divs = response.css('div.listcontainer')
        for div in divs:
            item = {}
            id = div.css('div.listcontainer div.row div.text-center a::attr(href)').extract_first().strip().replace('program-','')
            resp = requests.get('http://invest-tracing.com/detail-' + id)
            detailedresponse = BeautifulSoup(resp.text, features='lxml')
            item['name'] = detailedresponse.find('div',{'class' : 'col-sm-3 col-xs-6'}).find_all('a')[0].text.strip()
            item['url'] = self.get_url('http://invest-tracing.com/program-'+id)
            if(self.check_new(item['url'])):
                item['script'] = self.get_script(item['url'])
                self.generate_thumbnail(item['url'], item['name'])
                domain_details = self.get_domain(item['url'])
                if ('datacenter' in domain_details):
                    item['datacenter'] = domain_details['datacenter']
                    item['ip'] = domain_details['ip']
                    item['servers'] = domain_details['servers']
                item['Min/Max'] = detailedresponse.find('div',{'class' : 'col-sm-3 col-xs-6'}).find_all('strong')[1].text
                if (detailedresponse.find('label', {'class': 'label label-success'})):
                    item['status'] = 'Paying'
                elif (detailedresponse.find('label', {'class': 'label label-danger'})):
                    item['status'] = 'Not Paying'
                else:
                    item['status'] = 'Waiting'
                item['status_investtracing'] = item['status']
                item['withdrawtype'] = detailedresponse.find('label' , {'class' : 'label label-default bold'}).text
                item['investmentplan'] = detailedresponse.find('span',{'class': 'plan bold'}).text
                divs = detailedresponse.find_all('div',{'class' : 'col-sm-3 col-xs-6'})
                item['lastpaid'] = divs[1].find_all('br')[2].nextSibling.strip()
                div1 = detailedresponse.find_all('div',{'class' : 'row topborder toppad10 bottomspace10'})[1]
                div2 = div1.find('div',{'class' : 'col-sm-12'})
                imgs = div2.findChildren('img')
                takes = []
                for img in imgs:
                    if ('script' in img.attrs['title']):
                        continue
                    else:
                        takes.append(img.attrs['title'])
                item['takes'] = takes
                obj = self.scrape_ssl(item['url'])
                if ('valid' in obj):
                    item['valid'] = obj['valid']
                    item['issuerbrand'] = obj['issuerbrand'][0]
                    item['validation_type'] = obj['validation_type']

                item['forums'] = self.get_forums(item['url'])
                obj1 = self.get_buttons_and_statuses(item['url'])
                item['buttons'] = obj1['buttons']
                item['status1'] = obj1['status1']
                item['status2'] = obj1['status2']
                item['created_date'] = self.get_created_date(item['url'])
                button_resp = requests.get('http://invest-tracing.com/detail-' + id)
                button_response = BeautifulSoup(button_resp.text, features='lxml')
                item['button_investtracing'] = button_response.find('div',{'class':'col-md-8 col-sm-12'}).find('code').text.encode(
                    'utf-8')
                resp = requests.get('http://invest-tracing.com/rcb-'+id)
                item['RCB_investtracing'] = self.get_RCB_investtracing(resp)
                item['RCB_data_investtracing'] = self.get_RCB_data_investtracing(resp)


                yield item
