# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider

class allhyipslean(BaseSpider):
    name = 'allhyipslean'
    allowed_domains = ['all-hyips.info']
    start_urls = ['https://all-hyips.info/?lang=default']

    def start_requests(self):
        urls = [
            'https://all-hyips.info/?lang=default',
            'https://all-hyips.info/home/type/6/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        resp_soup = BeautifulSoup(response.text, features='lxml')
        divs = resp_soup.find_all('div', {'class': 'program'})
        for div in divs:
            item = {}
            # try:
            id = div.find('div', {'class': 'title'}).find('a').attrs['href'].split('/')[5]
            item['url'] = self.get_url('https://all-hyips.info/go/lid/' + id)

            item['status'] = div.find('div', {'class': 'status'}).text
            item['lastpaid'] = div.find('div', {'class': 'lastpayout'}).find('b').text
            item['status_allhyips'] = div.find('div', {'class': 'status'}).text
            item['button_allhyips'] = self.get_code('https://all-hyips.info/get_code/lid/' + id)[0].encode(
                'utf-8')

            resp = requests.get('https://all-hyips.info/refback/lid/' + id + '/')
            item['RCB_allhyips'] = self.get_RCB_classformnosize(resp)
            item['RCB_data_allhyips'] = self.get_RCB_data_classlist(resp)

        # except:
        #   pass

            yield item



