# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from hyipscrawl.spiders import BaseSpider


class hyipcruiserlean(BaseSpider):
    name = 'hyipcruiserlean'
    allowed_domains = ['hyip-cruiser']
    start_urls = ['http://hyip-cruiser.com/']

    def start_requests(self):
        urls = [
            'http://hyip-cruiser.com/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        resp_soup = BeautifulSoup(response.text, features='lxml')
        divs = resp_soup.find_all('div', {'class': 'program'})
        for div in divs:
            item = {}
            try:
                id = div.find('div',{'class' : 'title'}).find('a').attrs['href'].split('/')[5]
                item['url'] = self.get_url('http://hyip-cruiser.com/go/lid/'+id)
                classes_stat = div.find('div', {'class': 'status'}).attrs['class']
                for classe_stat in classes_stat:
                    if ('status1' in classe_stat):
                        item['status'] = 'Paying'
                    if ('status2' in classe_stat):
                        item['status'] = 'Waiting'
                item['status_hyipcruiser'] = item[
                    'status']  # div.find('div', {'class': 'status'}).xpath("@class").extract()
                item['lastpaid'] = div.find('div', {'class' : 'lastpayout'}).find('b').text
                item['button_hyipcruiser'] = self.get_code('https://hyip-cruiser.com/get_code/lid/' + id)[0].encode(
                    'utf-8')

                resp = requests.get('http://hyip-cruiser.com/?a=refback&lid=' + id + '/')
                item['RCB_hyipcruiser'] = self.get_RCB_classformnosize(resp)
                item['RCB_data_hyipcruiser'] = self.get_RCB_data_classlist(resp)
            except:
                pass

            yield item


    def get_RCB(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text, features = 'lxml')
        table = response.find('table',{'class' : 'form nosize'})
        if (table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        rcb['min'] = td_list[0].text
                        rcb['max'] = td_list[1].text
                        rcb['percentage'] = td_list[2].text
                        rcb['1stDepRCB'] = td_list[3].text
                        rcb['1stbonus'] = td_list[4].text
                        rcb['redep-rcb'] = td_list[5].text
                        rcb['redep-bonus'] = td_list[6].text
                        RCB.append(dict(rcb))
                    except:
                        pass
        return RCB

    def get_RCB_data(self,resp):
        RCB = []
        response = BeautifulSoup(resp.text, features = 'lxml')
        table = response.find('table',{'class':'list'})
        if(table is not None):
            for tr in table.find_all('tr'):
                rcb = {}
                td_list = tr.find_all("td")
                if len(td_list) > 0:
                    try:
                        rcb['date'] = td_list[0].text
                        rcb['depo'] = td_list[2].text.split('/')[0].replace('$','')
                        rcb['user'] = td_list[1].text
                        RCB.append(dict(rcb))
                    except:
                        pass
        return RCB

    #
    # def get_buttons(self,name):
    #     resp = requests.get('http://www.allhyipmonitors.com/details/' + name)
    #     buttons = []
    #     response = BeautifulSoup(resp.text, features = 'lxml')
    #     div = response.find('div',{'class':'mtd_wrap'})
    #     if(div is not None):
    #         divs = div.find_all('div',{'class' : 'mtd'})
    #         for div in divs:
    #             button = div.find('a',{'class':'mbutton'})
    #             buttons.append(button.encode('utf-8').decode('unicode_escape'))
    #     return buttons
    #
    # def get_created_date(self, name):
    #     resp = requests.get('http://allmonitors.net/hyip/' + name)
    #     response = BeautifulSoup(resp.text, features='lxml')
    #     if (response.find('div', {'class': 'hyipinfo nomargin'}) is not None):
    #         created_date = \
    #         response.find('div', {'class': 'hyipinfo nomargin'}).find('table').find_all('tr')[1].find_all('td')[
    #             1].text.split('-')
    #         return datetime.datetime(int(created_date[0]), int(created_date[1]), int(created_date[2]))
    #     else:
    #         return 'N/A'
    #
    # def scrape_ssl(self,url):
    #     payload = {'domain': url, 'port': '443'}
    #     obj = {}
    #     resp = requests.post('https://cryptoreport.geotrust.com/chainTester/webservice/validatecerts/json',payload)
    #     json_data =  json.loads(resp.text)
    #     try:
    #         cert_list = json_data['certAlgList'][0]['certList'][0]
    #         obj['valid'] =  cert_list['validTo'] #table.find_all('tr')[7].find('td').find('span',{'class' : 'comment'}).text
    #         obj['issuerbrand'] = cert_list['issuedByOrg']#table.find_all('tr')[10].find_all('td')[0].text
    #         obj['validation_type'] = cert_list['productType']#table.find_all('tr')[11].find('td').find('span',{'class' : 'bad'}).text
    #     except:
    #         pass
    #
    #     return obj