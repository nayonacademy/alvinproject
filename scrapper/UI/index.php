﻿<?php 
$connection = new MongoClient( "mongodb://127.0.0.1" );

// Suppose you have a collection called 'collection_name', then you can get its object using this code.
$db = $connection->hyip_1;

// Suppose you have a collection called 'collection_name.group', where 'group' is the named group of this collection, then you can get its object using this code.
$collection = $db->scrapper;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
$name = $_POST['hyip'];
$regex = new MongoRegex("/".$name."/i");
$cursor = $collection->find(array('name' => $regex))->sort(array('created_date'=>-1));
}
else
	$cursor = $collection->find()->sort(array('created_date'=>-1));?> 
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" href="https://hyiplist.org/project/css/bootstrap.css">
<link rel="stylesheet" href="https://hyiplist.org/project/css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link rel="icon" type="image/png" href="" />
<script src="https://hyiplist.org/project/js/jquery.js" type="597bd904948b2c6158a8aeb1-text/javascript"></script>
<script src="https://hyiplist.org/project/js/bootstrap.js" type="597bd904948b2c6158a8aeb1-text/javascript"></script>
<script src="https://use.fontawesome.com/9fac469b18.js" type="597bd904948b2c6158a8aeb1-text/javascript"></script>
<title>HYIP Monitor</title>
</head>
<body>
<nav class="navbar navbar-default navigation-clean-button">
<div class="container-fluid">
<div class="navbar-header" style="margin-left:20px"><a class="navbar-brand logo" href="#">HYIP Monitor</a>
<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
</div>
<div class="collapse navbar-collapse navbar-right" id="navcol-1">
<ul class="nav navbar-nav">
<li role="presentation"><a href="index.php"><i style="margin-right:2px" class="fa fa-home fa-fw" aria-hidden="true"></i>HOME</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-rss fa-fw" aria-hidden="true"></i>BLOG</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-plus fa-fw" aria-hidden="true"></i>ADD HYIP</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-th-large fa-fw" aria-hidden="true"></i>ADD BANNER</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-comments-o fa-fw" aria-hidden="true"></i>FAQ</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-envelope-o fa-fw" aria-hidden="true"></i>CONTACT US</a></li>
</ul>
</div>
</div>
</nav>
<div class="container-fluid main">
<div class="main-content">
<div class="row">
<div class="col-md-12">
</div>
</div>
</div>
<div class="row">
</div>
<br>
<div class="row left">
<div class="col-md-3 col3">
<div class="leftbox">
<div class="leftboxhead">
<i class="fa fa-history blue"></i> Top Programs
</div>
<div class="leftboxcontent">
<div class="row leftbuttons">
</div>
<br>
<div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="all">
</div>
<div role="tabpanel" class="tab-pane" id="today">
</div>
<div role="tabpanel" class="tab-pane" id="yes">
</div>
</div>
</div>
</div>
<br>
<div class="leftbox">
<div class="leftboxhead">
<i class="fa fa-history blue"></i> Top Monitors
</div>
<div class="leftboxcontent">
<div class="row leftbuttons">
<ul class="nav nav-tabs" role="tablist" style="border:none">
<div class="col-md-4 but">
<a href="#all2" aria-controls="home" role="tab" data-toggle="tab" class="btn btn-primary">Today</a>
</div>
<div class="col-md-4 but">
</div>
<div class="col-md-4 butt">
<a href="#yes2" aria-controls="home" role="tab" data-toggle="tab" class="btn btn-primary">Yetserday</a>
</div>
</ul>
</div>
<br>
<div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="all2">
</div>
<div role="tabpanel" class="tab-pane" id="yes2">
</div>
</div>
</div>
</div>
</div>
<div class="col-md-5 col5">
<div id="custom-search-input">
	<form action="index.php" method="POST">
		<div class="input-group col-md-12">
			<input name="hyip" type="text" class="  search-query form-control" placeholder="Search hyips.." />
			<span class="input-group-btn"><button name="submit" class="btn btn-danger" type="submit"><span class=" glyphicon glyphicon-search"></span></button></span>
		</div>
	</form>
</div>
<div style="clear:both"></div><br>
<?php
foreach ( $cursor as $id => $value )
{  
	//var_dump( $value );
	$name = $value['name'];
	$url= $value['url'];
    $thumbnail = $value['thumbnail'];
	$last_paid = $value['lastpaid'];
	if(isset($value['created_date']) && $value['created_date'] != 'N/A'){
	   $created_date = date('Y-m-d', $value['created_date']->sec);//echo $created_date;
	   $now = time(); // or your date as well
       $datediff = $now - strtotime($created_date); //echo $now;
       $lifetime = round($datediff / (60 * 60 * 24));
	}
    else {
		$created_date = 'n/a';
	$lifetime = 'n/a';}
	
	

	$takes = $value['takes'];
	if(isset($value['withdrawtype']))
		$withdrawl = $value['withdrawtype'];
	else 
		$withdrawl = 'n/a';

	$min_depo = $value['Min/Max'];
	$status = $value['status'];
	if(isset($value['investmentplan']))
		$investment_plan=$value['investmentplan'];
	else
		$investment_plan = 'n/a';
	if(isset($value['valid']))
		$ssl_valid = $value['valid'];
	else 
		$ssl_valid = 'n/a';
	if(isset($value['issuerbrand']))
		if(is_array($value['issuerbrand']))
			$issuer_brand = $value['issuerbrand'][0];
		else
			$issuer_brand = $value['issuerbrand'];
	else
		$issuer_brand = '';
	if(isset($value['validation_type']))
		$validation_type = $value['validation_type'];
	else
		$validation_type = 'n/a';
	if(isset($value['script']))
		$script = $value['script'];
	else
		$script = 'n/a';
	if(isset($value['datacenter']))
		$datacenter = $value['datacenter'];
	else
		$datacenter = 'n/a';
	if(isset($value['ip']))
		$ip = $value['ip'];
	else
		$ip = 'n/a';
	if(isset($value['servers']))
		$servers = $value['servers'];
	else
		$servers = 'n/a';
	if(isset($value['status1']))
		$status1 = $value['status1'];
	else
	    $status1 = '';
	if(isset($value['status2']))
		$status2 = $value['status2'];
	else
	    $status2 = '';

	$forums = [];
	if(isset($value['forums']))
        $forums = $value['forums'];
	
	
	$img = "https://blinky.nemui.org/shot?".$url;
  $monitors =  array ('fairmonitor', 'graspgold','investtracing','myhyips','hyipclub','allhyips','uhyips','hyipcruiser','elitemonitor');

?>
<div class="rightbox">
<div class="rightboxhead">
<i class="fa fa-external-link"></i> <a id="righttitle" href="<?php echo $url; ?>" target="_blank"><?php echo $name; ?></a>
<span class="floatright">
<i class="fa fa-ban"></i> <a href="">Scam report</a>
</span>
<span class="floatrightdetails">
<i class="fa fa-info-circle"></i> <a href="/hyiplist/programdetails.php?id=<?php echo $name;?>">Program Details</a>
</span>
</div>
<div class="rightboxcontent">
<div class="row rightrow">
<div class="col-md-2 hyipimage" id="hyipimage5078">
<img src="<?php echo $img;?>" onerror="this.onerror=null;this.src='http://autosparescentral.com/scraper/project/images/default.gif'">
</div>
<div class="col-md-2 hyipimage hyipimage2" id="hyipimage25078">
<img src="images/default.gif">
</div>
<div class="col-md-5">
<div class="hyipcontent">
<div class="data">» Monitor Since : <strong><?php echo $created_date;?> (<?php echo $lifetime; ?> days)</strong></div>
<div class="data">» Withdrawal : <strong><?php echo $withdrawl;?></strong></div>
<div class="data">» Minimum Deposit : <strong><?php echo $min_depo; ?></strong></div>
<div class="data">» Last Payout : <span style="color:#61a38c"><strong><?php echo $last_paid;?> </strong></span></div>
<div class="data">» Accept : <?php foreach ( $takes as $take ) {echo $take; }?>, </div>
<div class="data">» status count<strong>
<?php $paying = 0; $waiting = 0; $problem = 0; $notpaying = 0; 
foreach($monitors as $monitor) { 
if(isset($value['status_'.$monitor]) ){ 
if(strtolower(trim($value['status_'.$monitor])) === 'paying') $paying = $paying+1; 
if(strtolower(trim($value['status_'.$monitor])) === 'waiting') $waiting = $waiting+1; 
if(strtolower(trim($value['status_'.$monitor])) === 'problem') $problem = $problem+1; 
if(strtolower(trim($value['status_'.$monitor])) === 'notpaying') $notpaying = $notpaying+1; 
}
}
if($paying > 0) echo 'Paying ('.$paying.')';
if($waiting > 0) echo 'Waiting ('.$waiting.')';
if($problem > 0) echo 'Problem ('.$problem.')';
if($notpaying > 0) echo 'Not Paying ('.$notpaying.')';
?> </strong></div>
<div class="data">» ssl valid : <strong><?php echo $ssl_valid;?> </strong></div>
<div class="data">» issuer brand: <strong><?php echo $issuer_brand;?> </strong></div>
<div class="data">» validation type : <strong><?php echo $validation_type;?> </strong></div>
<div class="data">» script : <strong><?php echo $script;?> </strong></div>
<div class="data">» datacenter : <strong><?php echo $datacenter;?> </strong></div>
<div class="data">» IP : <strong><?php echo $ip;?> </strong></div>
<div class="data">» servers : <strong><?php echo $servers;?> </strong></div>
<div class="data">» forums : <strong>
<?php foreach ( $forums as $forum ) { $full_link =  $forum; $arr  = explode("/", $forum);$display_name = $arr[2];?><a href="<?php echo $forum;?>"><?php echo $display_name;?></a>, &nbsp;<?php } ?> </strong></div>
</div>
</div>
<div class="col-md-4">
<div class="paying">
<div class="payingboxgreen"><?php echo $status;?></div>
<div class="payingboxcontent">
<div class="data">» Monitor Count : <strong>4</strong></div>
<div class="data">» Total Deposit : <strong></strong></div>
</div>
</div>
</div>
</div>
<div class="plan">
<i class="fa fa-line-chart arrow"></i> <span>Investment Plan: <strong><?php echo $investment_plan;?></strong></span>
</div>
</div>
</div>
<br>
<?php } ?>

<div class="row"><div class="col-md-12 text-center"></div></div>
</div>
</div>
</div><br>
<div class="footer">
<div class="footerinner">
Disclaimer: Please bear in mind that all HYIPs investments presuppose high risks. We do not promote or endorse any programs listed here. Some programs may be illegal depending on your country's laws. Past Performance of any of programs is no guarantee for the same or similar future performance. Paying status and others status is for this monitor not for your. We don't give practice, all investments decisions are yours. DON'T SPEND WHAT YOU CAN'T AFFORD TO LOSE!
<br><br>Copyright &copy; HYIPMONITOR 2017 All rights reserved
</div>
</div>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/2448a7bd/cloudflare-static/rocket-loader.min.js" data-cf-nonce="597bd904948b2c6158a8aeb1-" defer=""></script></body>
</html>