﻿<?php 
$connection = new MongoClient( "mongodb://127.0.0.1" );

// Suppose you have a collection called 'collection_name', then you can get its object using this code.
$db = $connection->hyip_1;
// Suppose you have a collection called 'collection_name.group', where 'group' is the named group of this collection, then you can get its object using this code.
$collection = $db->scrapper;
$cursor = $collection->find()->sort(array('created_date'=>-1));
$monitors = ['fairmonitor', 'graspgold','investtracing','myhyips','hyipclub','allhyips','uhyips','hyipcruiser','elitemonitor','myinvestblog'];
$search_name = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
$search_name = $_POST['hyip'];
$regex = new MongoRegex("/".$search_name."/i");
$filteredcursor = $collection->find(array('name' => $regex))->sort(array('created_date'=>-1));
$cursor_arr_filtered = array();
foreach ( $filteredcursor as $id => $value ){
$total = 0; $buttonscount = 0;$totaltoday=0;$totalyesterday=0;
$strtotime_today = strtotime('today UTC'); $strtotime_yesterday = strtotime('-1 day');
foreach($monitors as $monitor) {
	if(isset($value['RCB_data_'.$monitor])){       
		$rcbs_data[$monitor] = $value['RCB_data_'.$monitor];
		foreach($rcbs_data[$monitor] as $data){ //var_dump($data);
			if($monitor === 'uhyips'){
				//$depo_date = substr($data['date'], 0, 11); 
                		$dd = DateTime::createFromFormat('d M, Y',substr($data['date'], 0, 11));
				$depo_date = $dd->format('M dS, Y'); //echo $dd->format('d M,Y').'/</br>';
			}
			else
				$depo_date = substr($data['date'], 0, -9);
			$strtime = strtotime($depo_date);		
                    	$depo = $data['depo'];
			if($strtime == $strtotime_today)
		    	$totaltoday = floatval(str_replace(",","",$totaltoday)) + floatval(str_replace(",","",$depo)); 
		    	if($strtime == $strtotime_yesterday)
		    	$totalyesterday = floatval(str_replace(",","",$totalyesterday)) + floatval(str_replace(",","",$depo)); 
		    	$total = floatval(str_replace(",","",$total)) + floatval(str_replace(",","",$depo));
		}
     	}
	if(isset($value['button_'.$monitor])) $buttonscount++;
 }
$value['totaldeposit'] = $total;
$value['totaltoday'] = $totaltoday;
$value['totalyesterday'] = $totalyesterday;
$value['buttonscount'] = $buttonscount;
array_push($cursor_arr_filtered ,$value);
}
}

if(isset($_GET['page'])){
  $from = $_GET['page']*10;

}
else {
$from = 0 ; 
}
$cursor_arr = array();
$strtotime_today = strtotime('00:00'); $strtotime_yesterday = strtotime(date('d.m.Y',strtotime('-1 day')));


foreach ( $cursor as $id => $value ){
$total = 0; $buttonscount = 0;
$totaltoday=0;$totalyesterday=0;
foreach($monitors as $monitor) {
	
	if(isset($value['RCB_data_'.$monitor])){       
		$rcbs_data[$monitor] = $value['RCB_data_'.$monitor];
		foreach($rcbs_data[$monitor] as $data){ //var_dump($data);
			if($monitor === 'uhyips'){
				//$depo_date = substr($data['date'], 0, 11); 
                		$dd = DateTime::createFromFormat('d M, Y',substr($data['date'], 0, 11));
				$depo_date = $dd->format('M dS, Y'); //echo $dd->format('d M,Y').'/</br>';
			}
			else
				$depo_date = substr($data['date'], 0, -9);
			$strtime = strtotime($depo_date);
                    	$depo = $data['depo'];
			if($strtime === $strtotime_today)
		    	$totaltoday = floatval(str_replace(",","",$totaltoday)) + floatval(str_replace(",","",$depo)); 
		    	if($strtime === $strtotime_yesterday)
		    	$totalyesterday = floatval(str_replace(",","",$totalyesterday)) + floatval(str_replace(",","",$depo)); 
		    	$total = floatval(str_replace(",","",$total)) + floatval(str_replace(",","",$depo));
		 }
     	}
	if(isset($value['button_'.$monitor])) $buttonscount++;
 

}
$value['totaldeposit'] = $total;
$value['totaltoday'] = $totaltoday;
$value['totalyesterday'] = $totalyesterday;
$value['buttonscount'] = $buttonscount;
array_push($cursor_arr,$value);
}

$totaldeposorted = $cursor_arr; usort( $totaldeposorted,function($a,$b){ 
return (floatval($a['totaldeposit']) <= floatval($b['totaldeposit'])) ? 1 : -1; });
$totaltodaysorted = $cursor_arr; usort( $totaltodaysorted,function($a,$b){ 
return (floatval($a['totaltoday']) <= floatval($b['totaltoday'])) ? 1 : -1; });
$totalyesterdaysorted = $cursor_arr; usort( $totalyesterdaysorted,function($a,$b){ 
return (floatval($a['totalyesterday']) <= floatval($b['totalyesterday'])) ? 1 : -1; });

$totalbuttonssorted = $cursor_arr; usort( $totalbuttonssorted,function($a,$b){ 
if(floatval($a['buttonscount']) == floatval($b['buttonscount'])) return 0;
return floatval($a['buttonscount']) < floatval($b['buttonscount']) ? 1:-1; });
$display_arr =array_slice($cursor_arr,$from,10);
$total_hyip_count = count($cursor_arr);
if ($_SERVER['REQUEST_METHOD'] === 'POST'){
$display_arr = array_slice($cursor_arr_filtered,$from,10);
$total_hyip_count = count($cursor_arr_filtered);
}
?> 
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" href="https://hyiplist.org/project/css/bootstrap.css">
<link rel="stylesheet" href="https://hyiplist.org/project/css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link rel="icon" type="image/png" href="" />

<script src="https://hyiplist.org/project/js/jquery.js" type="6c5c4595c28eede4f38c75ac-text/javascript"></script>

<script src="https://hyiplist.org/project/js/bootstrap.js" type="597bd904948b2c6158a8aeb1-text/javascript"></script>
<script src="https://use.fontawesome.com/9fac469b18.js" type="597bd904948b2c6158a8aeb1-text/javascript"></script>
<title>HYIP Monitor</title>
</head>
<body>
<nav class="navbar navbar-default navigation-clean-button">
<div class="container-fluid">
<div class="navbar-header" style="margin-left:20px"><a class="navbar-brand logo" href="#">HYIP Monitor</a>
<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
</div>
<div class="collapse navbar-collapse navbar-right" id="navcol-1">
<ul class="nav navbar-nav">
<li role="presentation"><a href="indexpagination.php"><i style="margin-right:2px" class="fa fa-home fa-fw" aria-hidden="true"></i>HOME</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-rss fa-fw" aria-hidden="true"></i>BLOG</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-plus fa-fw" aria-hidden="true"></i>ADD HYIP</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-th-large fa-fw" aria-hidden="true"></i>ADD BANNER</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-comments-o fa-fw" aria-hidden="true"></i>FAQ</a></li>
<li role="presentation"><a href="#"><i style="margin-right:2px" class="fa fa-envelope-o fa-fw" aria-hidden="true"></i>CONTACT US</a></li>
</ul>
</div>
</div>
</nav>
<div class="container-fluid main">
<div class="main-content">
<div class="row">
<div class="col-md-12">
</div>
</div>
</div>
<div class="row">
</div>
<br>
<div class="row left">
<div class="col-md-3 col3">
<div class="leftbox">
<div class="leftboxhead">
<i class="fa fa-history blue"></i> Top Programs
</div>
<div class="leftboxcontent">
<div class="row leftbuttons">
<ul class="nav nav-tabs" role ="tablist" style="border:none;">
<div class='col-md-4 but'>
<a href='#all' role='tab1' class='btn btn-primay'> All Time</a>
</div>
<div class='col-md-4 but'>
<a href='#yest' role='tab1' class='btn btn-primay'> Yesterday</a>
</div>
<div class='col-md-4 but'>
<a href='#today' role='tab1' class='btn btn-primay'> Today</a>
</div>
</ul>
</div>
<br>
<div class="tab-content">
<div role="tabpanel1" class="tab-pane active" id="all">
<?php $counter =0;
 foreach( $totaldeposorted as $id1=>$value1){
if($counter < 11){
$name = $value1['name']; $url = $value1['url']; $totaldepo = (int) $value1['totaldeposit'];
?>
<div class='lefthyip'>
<a href='/hyiplist/programdetails.php?id=<?php echo str_replace('/','_',$url);?>'><?php echo $name;?><span><?php echo $totaldepo;?></span>
</div>
<?php }$counter++;}?>

</div>
<div role="tabpanel1" class="tab-pane" id="yest">
<?php $counter =0;
 foreach( $totalyesterdaysorted as $id1=>$value1){
$name = $value1['name']; $url = $value1['url']; $totaldepo = (int) $value1['totalyesterday'];
if($counter < 11 && $totaldepo > 0){
?>
<div class='lefthyip'>
<a href='/hyiplist/programdetails.php?id=<?php echo str_replace('/','_',$url);?>'><?php echo $name;?><span><?php echo $totaldepo;?></span>
</div>
<?php }$counter++;}?>

</div>
<div role="tabpanel1" class="tab-pane" id="today">
<?php $counter =0;
 foreach( $totaltodaysorted as $id1=>$value1){
$name = $value1['name']; $url = $value1['url']; $totaldepo = (int)$value1['totaltoday'];
if($counter < 11 && $totaldepo > 0){
?>
<div class='lefthyip'>
<a href='/hyiplist/programdetails.php?id=<?php echo str_replace('/','_',$url);?>'><?php echo $name;?><span><?php echo $totaldepo;?></span></a>
</div>
<?php }$counter++;}?>

</div>
</div>
</div>
</div>
<br>
<div class="leftbox">
<div class="leftboxhead">
<i class="fa fa-history blue"></i> Top Monitors
</div>
<div class="leftboxcontent">
<div class="row leftbuttons">
<ul class="nav nav-tabs" role="tablist" style="border:none">
<div class="col-md-4 but">
</div>
<div class="col-md-4 but">
</div>
<div class="col-md-4 butt">
</div>
</ul>
</div>
<br>
<div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="all2">
<?php $counter =0;
 foreach( $totalbuttonssorted as $id1=>$value1){$counter++;
if($counter < 21){
$name = $value1['name']; $url = $value1['url']; $total = $value1['buttonscount'];
?>
<div class='lefthyip'>
<a href='/hyiplist/programdetails.php?id=<?php echo str_replace('/','_',$url);?>'><?php echo $name;?><span><?php echo $total;?></span></a>
</div>
<?php }}?>

</div>
<div role="tabpanel" class="tab-pane" id="yes2">
</div>
</div>
</div>
</div>
</div>
<div class="col-md-5 col5">
<div id="">
	<form action="indexpagination.php" method="POST">
		<div class="input-group col-md-12">
			<input name="hyip" type="text" value='<?php echo $search_name;?>' autocomplete='off' class="form-control" placeholder="Search hyips.." />
			<span class="input-group-btn"><button name="submit" class="btn btn-danger" type="submit">Search</button></span>
		</div>
	</form>
</div>
<br>
<?php
foreach ( $display_arr as $id => $value )
{  
	//var_dump( $value );
	$name = $value['name'];
	$url= $value['url'];
    //$thumbnail = $value['thumbnail'];
	$last_paid = $value['lastpaid'];
	if(isset($value['created_date']) && $value['created_date'] != 'N/A'){
	   $created_date = date('Y-m-d', $value['created_date']->sec);//echo $created_date;
	   $now = time(); // or your date as well
       $datediff = $now - strtotime($created_date); //echo $now;
       $lifetime = round($datediff / (60 * 60 * 24));
	}
    else {
		$created_date = 'n/a';
	$lifetime = 'n/a';}
	
	

	$takes = $value['takes'];
	if(isset($value['withdrawtype']))
		$withdrawl = $value['withdrawtype'];
	else 
		$withdrawl = 'n/a';
	$min_depo = $value['Min/Max'];
	$status = $value['status'];
	if(isset($value['investmentplan']))
		$investment_plan=$value['investmentplan'];
	else
		$investment_plan = 'n/a';
	if(isset($value['valid']))
		$ssl_valid = $value['valid'];
	else 
		$ssl_valid = 'n/a';
	if(isset($value['issuerbrand']))
		if(is_array($value['issuerbrand']))
			$issuer_brand = $value['issuerbrand'][0];
		else
			$issuer_brand = $value['issuerbrand'];
	else
		$issuer_brand = '';
	if(isset($value['validation_type']))
		$validation_type = $value['validation_type'];
	else
		$validation_type = 'n/a';
	if(isset($value['script']))
		$script = $value['script'];
	else
		$script = 'n/a';
	if(isset($value['datacenter']))
		$datacenter = $value['datacenter'];
	else
		$datacenter = 'n/a';
	if(isset($value['ip']))
		$ip = $value['ip'];
	else
		$ip = 'n/a';
	if(isset($value['servers']))
		$servers = $value['servers'];
	else
		$servers = 'n/a';
	if(isset($value['status1']))
		$status1 = $value['status1'];
	else
	    $status1 = '';
	if(isset($value['status2']))
		$status2 = $value['status2'];
	else
	    $status2 = '';

	$forums = [];
	if(isset($value['forums']))
        $forums = $value['forums'];
	
	
	$img = "https://blinky.nemui.org/shot?https://".$url;
$totaldepo = $value['totaldeposit'];
$totaltoday = $value['totaltoday']; $totalyesterday = $value['totalyesterday'];
//  $monitors =  array ('fairmonitor', 'graspgold','investtracing','myhyips','hyipclub','allhyips','uhyips','hyipcruiser','elitemonitor');
$domain = str_replace('/','_',$url);
?> 
<div class="rightbox">
<div class="rightboxhead">
<i class="fa fa-external-link"></i> <a id="righttitle" href="http://<?php echo $url; ?>" target="_blank"><?php echo $name; ?></a>
<span class="floatright">
<i class="fa fa-ban"></i> <a href="">Scam report</a>
</span>
<span class="floatrightdetails">
<i class="fa fa-info-circle"></i> <a href="/hyiplist/programdetails.php?id=<?php echo $domain;?>">Program Details</a>
</span>
</div>
<div class="rightboxcontent">
<div class="row rightrow">
<div class="col-md-2 hyipimage" id="hyipimage5078">
<img src="<?php echo $img;?>" onerror="this.onerror=null;this.src='http://autosparescentral.com/scraper/project/images/default.gif'">
</div>
<div class="col-md-2 hyipimage hyipimage2" id="hyipimage25078">
<img src="images/default.gif">
</div>
<div class="col-md-5">
<div class="hyipcontent">
<div class="data">» Monitor Since : <strong><?php echo $created_date;?> (<?php echo $lifetime; ?> days)</strong></div>
<div class="data">» Withdrawal : <strong><?php echo $withdrawl;?></strong></div>
<div class="data">» Minimum Deposit : <strong><?php echo $min_depo; ?></strong></div>
<div class="data">» Last Payout : <span style="color:#61a38c"><strong><?php echo $last_paid;?> </strong></span></div>
<div class="data">» Accept : <?php foreach ( $takes as $take ) {echo $take; }?>, </div>
<div class="data">» status count<strong>
<?php $paying = 0; $waiting = 0; $problem = 0; $notpaying = 0; 
foreach($monitors as $monitor) { 
if(isset($value['status_'.$monitor]) ){ 
if(strtolower(trim($value['status_'.$monitor])) === 'paying') $paying = $paying+1; 
if(strtolower(trim($value['status_'.$monitor])) === 'waiting') $waiting = $waiting+1; 
if(strtolower(trim($value['status_'.$monitor])) === 'problem' || strtolower(trim($value['status_'.$monitor])) === 'problems') $problem = $problem+1; 
if(strtolower(trim($value['status_'.$monitor])) === 'not paying' || strtolower(trim($value['status_'.$monitor])) === 'scam' ) $notpaying = $notpaying+1; 
}
}
if($paying > 0) echo 'Paying ('.$paying.')';
if($waiting > 0) echo 'Waiting ('.$waiting.')';
if($problem > 0) echo 'Problem ('.$problem.')';
if($notpaying > 0) echo 'Not Paying ('.$notpaying.')';
?> </strong></div>
<div class="data">» ssl valid : <strong><?php echo $ssl_valid;?> </strong></div>
<div class="data">» issuer brand: <strong><?php echo $issuer_brand;?> </strong></div>
<div class="data">» validation type : <strong><?php echo $validation_type;?> </strong></div>
<div class="data">» script : <strong><?php echo $script;?> </strong></div>
<div class="data">» datacenter : <strong><?php echo $datacenter;?> </strong></div>
<div class="data">» IP : <strong><?php echo $ip;?> </strong></div>
<div class="data">» servers : <strong><?php echo $servers;?> </strong></div>
<div class="data">» forums : <strong>
<?php foreach ( $forums as $forum ) { $full_link =  $forum; $arr  = explode("/", $forum);$display_name = $arr[2];?><a href="<?php echo $forum;?>"><?php echo $display_name;?></a>, &nbsp;<?php } ?> </strong></div>
</div>
</div>
<div class="col-md-4">
<div class="paying">
<div class="payingboxgreen"><?php echo $status;?></div>
<div class="payingboxcontent">
<div class="data">» Monitor Count : <strong>4</strong></div>
<div class="data">» Total Deposit : <strong><?php echo $totaldepo;?></strong></div>
<div class="data">» Total Deposit yesterday: <strong><?php echo $totalyesterday;?></strong></div>
<div class="data">» Total Deposit today: <strong><?php echo $totaltoday;?></strong></div>
</div>
</div>
</div>
</div>
<div class="plan">
<i class="fa fa-line-chart arrow"></i> <span>Investment Plan: <strong><?php echo $investment_plan;?></strong></span>
</div>
</div>
</div>
<br>
<?php } ?>

<div class="row"><div class="col-md-12 text-center">
<ul class='pagination'>
<?php $totalpages = ceil($total_hyip_count/10); 
for ($i=0;$i<$totalpages;$i++){
	echo '<li> <a href="?page='.$i.'">'.$i.'</a></li>';
}
?>
</ul>
</div></div>
</div>
</div>
</div><br>
<div class="footer">
<div class="footerinner">
Disclaimer: Please bear in mind that all HYIPs investments presuppose high risks. We do not promote or endorse any programs listed here. Some programs may be illegal depending on your country's laws. Past Performance of any of programs is no guarantee for the same or similar future performance. Paying status and others status is for this monitor not for your. We don't give practice, all investments decisions are yours. DON'T SPEND WHAT YOU CAN'T AFFORD TO LOSE!
<br><br>Copyright &copy; HYIPMONITOR 2017 All rights reserved
</div>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type='text/javascript'>
$("a[role='tab1']").click(function(event){ 
var target = $(this).attr('href');
$('div[role="tabpanel1"]').removeClass('active');
$(target).addClass('active');
return false;
});
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/2448a7bd/cloudflare-static/rocket-loader.min.js" data-cf-nonce="597bd904948b2c6158a8aeb1-" defer=""></script></body>
</html>