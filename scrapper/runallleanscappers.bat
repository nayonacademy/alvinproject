@echo off
for %%x in (
        allhyipslean
        elitemonitorlean
	    fairmonitorlean
        graspgoldlean
		hyipclublean
		hyipcruiserlean
		investtracinglean
		myhipslean
		uhyipslean
		myivestbloglean
       ) do (
         echo starting scapper %%x
         scrapy crawl %%x
         echo =-=-=-=-=-=
         echo.
       )